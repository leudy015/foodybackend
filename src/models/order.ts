import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const orderSchema = new mongoose.Schema(
  {
    restaurant: {
      type: String,
    },

    userID: {
      type: String,
    },

    aceptaTerminos: {
      type: Boolean,
    },

    propina: {
      type: Boolean,
    },

    cupon: { type: mongoose.Schema.Types.ObjectId, ref: "cupones" },

    cantidad: { type: Number, default: 1 },

    pagoPaypal: { type: mongoose.Schema.Types.Mixed },

    stripePaymentIntent: { type: mongoose.Schema.Types.Mixed },

    estado: {
      type: String,
      enum: [
        "Pendiente de pago",
        "Pagada",
        "Confirmada",
        "Preparando",
        "Lista para recojer",
        "Recogido",
        "Rechazada",
        "Devuelto",
      ],
      default: "Pendiente de pago",
    },

    progreso: {
      type: String,
      enum: ["0", "25", "50", "75", "100"],
      default: "25",
    },
    status: {
      type: String,
      enum: ["success", "exception", "normal", "active"],
      default: "active",
    },

    platos: {
      type: [mongoose.Schema.Types.Mixed],
    },
    time: {
      type: String,
    },

    nota: {
      type: String,
    },

    cubiertos: {
      type: Boolean,
    },

    total: {
      type: String,
    },

    isvalored: {
      type: Boolean,
      default: false,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

orderSchema.virtual("descuento", {
  ref: "cupones",
  localField: "cupon",
  foreignField: "_id",
  justOne: true,
});

export interface IOrder extends Document {
  restaurant: string;
  platos: String;
  time: string;
  created_at: Date;
}

export default mongoose.model<IOrder>("order", orderSchema);
