import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const AcompananteSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      text: true,
    },

    required: {
      type: Boolean,
      default: false,
    },

    children: [{ type: mongoose.Schema.Types.Mixed }],

    plato: {
      type: String,
      required: true,
    },

    restaurant: {
      type: String,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IAcompanante extends Document {
  name: string;
  children: string;
  plato: string;
  restaurant: string;
}

export default mongoose.model<IAcompanante>("acompanante", AcompananteSchema);
