import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const PlatoSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      text: true,
    },
    ingredientes: {
      type: String,
      text: true,
    },
    price: {
      type: String,
      required: true,
    },

    imagen: {
      type: String,
    },

    restaurant: {
      type: String,
    },

    menu: {
      type: String,
    },

    oferta: {
      type: Boolean,
      default: false,
    },

    popular: {
      type: Boolean,
      default: false,
    },

    news: {
      type: Boolean,
      default: false,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IPlato extends Document {
  user: string;
  comment: String;
  value: number;
}

export default mongoose.model<IPlato>("plato", PlatoSchema);
