import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const MenuSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      text: true,
      required: true,
    },

    subtitle: {
      type: String,
    },

    restaurant: {
      type: String,
      required: true,
    },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IMenu extends Document {
  user: string;
  comment: String;
  value: number;
}

export default mongoose.model<IMenu>("menu", MenuSchema);
