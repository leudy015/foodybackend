import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const cardSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },

  restaurant: {
    type: String,
    required: true,
  },

  plato: {
    type: mongoose.Schema.Types.Mixed,
    required: true,
  },

  complementos: [String],

  platoID: {
    type: String,
  },

  total: {
    type: String,
  },
  extra: {
    type: String,
  },
});

export interface ICard extends Document {
  userId: string;
  restaurant: String;
  plato: string;
  complementos: string;
}

export default mongoose.model<ICard>("card", cardSchema);
