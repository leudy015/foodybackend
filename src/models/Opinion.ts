import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const OpinionSchema = new mongoose.Schema(
  {
    plato: {
      type: String,
      required: true,
    },
    comment: {
      type: String,
      required: true,
    },
    rating: {
      type: Number,
      required: true,
    },

    user: {
      type: String,
      required: true,
    },
  },
  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface IOpinion extends Document {
  plato: string;
  comment: String;
  rating: number;
  user: string;
}

export default mongoose.model<IOpinion>("opinion", OpinionSchema);
