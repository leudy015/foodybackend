import mongoose, { Document } from "mongoose";
import bcrypt from "bcryptjs";

mongoose.Promise = global.Promise;

const restaurantSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    text: true,
  },
  image: {
    type: String,
    required: true,
    text: true,
  },
  description: {
    type: String,
    required: true,
  },

  rating: {
    type: String,
  },

  address: {
    type: mongoose.Schema.Types.Mixed,
  },

  coordetate: {
    type: String,
  },

  coordenateone: {
    type: String,
  },

  categoryName: {
    type: String,
  },

  categoryID: {
    type: String,
  },

  minime: {
    type: Number,
  },

  menu: {
    type: String,
  },

  phone: {
    type: String,
  },

  email: {
    type: String,
  },

  logo: {
    type: String,
  },

  password: {
    type: String,
  },

  city: {
    type: String,
  },

  type: {
    type: String,
  },

  apertura: {
    type: String,
  },

  cierre: {
    type: String,
  },

  diaslaborales: {
    type: [String],
  },

  isnew: {
    type: Boolean,
    default: true,
  },

  open: {
    type: Boolean,
    default: true,
  },

  OnesignalID: {
    type: String,
  },
});

export interface IRestaurant extends Document {
  email: string;
  password: string;
}

// hashear los password antes de guardar
restaurantSchema.pre<IRestaurant>("save", function (next) {
  // Si el password no esta hasheado...
  if (!this.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) return next(err);

    bcrypt.hash(this.password, salt, (err, hash) => {
      if (err) return next(err);
      this.password = hash;
      next();
    });
  });
});

export default mongoose.model<IRestaurant>("restaurant", restaurantSchema);
