import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const categoriaSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    text: true,
    unique: true,
  },
  image: {
    type: String,
    required: true,
    text: true,
  },
  description: {
    type: String,
    required: true,
    unique: true,
  },
});

export interface ICategory extends Document {
  title: string;
  image: String;
  description: string;
}

export default mongoose.model<ICategory>("categoria", categoriaSchema);
