import mongoose, { Document } from "mongoose";

mongoose.Promise = global.Promise;

const NotificationSchema = new mongoose.Schema(
  {
    user: { type: String },
    usuario: { type: String },
    restaurant: { type: String },
    type: { type: String },
    read: { type: mongoose.Schema.Types.Boolean, default: false },
    ordenId: { type: String },
  },

  { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } }
);

export interface INotification extends Document {
  user: string;
  restaurant: string;
  type: string;
  read: boolean;
  orderId: string;
}

export default mongoose.model<INotification>(
  "notification",
  NotificationSchema
);
