import { Response, Router } from "express";
import userSchema from "../models/user";
import restaurantSchema from "../models/restaurant";
import crypto from "crypto";
import bcrypt from "bcryptjs";
const nodemailer = require("nodemailer");
import recoverPasswordEmail from "../RecoveryPassword/Email/RecoveryPassword";
import recoverPasswordEmailRestaurant from "../RecoveryPassword/Email/RecoveryPasswordrestaurant";

class RecoveryRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/forgotpassword", async (req: any, res: Response) => {
      const email = req.query.email;
      await userSchema.find({ email: email }, (err: any, data: any) => {
        if (data.length < 1 || err) {
          res.status(400).json({
            message: "Aún no tenemos este correo eletrónico",
            success: false,
            noEmail: true,
          });
        } else {
          const token = crypto.randomBytes(20).toString("hex");
          // write to database
          const transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            service: "gmail",
            port: 465,
            secure: true,
            auth: {
              user: process.env.EMAIL_ADDRESS,
              pass: process.env.EMAIL_PASSWORD,
            },
          });
          const mailOptions = {
            from: process.env.EMAIL_ADDRESS,
            to: email,
            subject: "Recuperar contraseña Foody App",
            text: "Recuperar contraseña Foody App",
            html: recoverPasswordEmail(token),
          };
          transporter.sendMail(mailOptions, (err: any) => {
            if (err) {
              console.log("err:", err);
            } else {
              var newvalues = { $set: { forgotPasswordToken: token } };
              userSchema.findOneAndUpdate(
                { email: email },
                newvalues,
                {
                  //options
                  new: true,
                  strict: false,
                  useFindAndModify: false,
                },
                (err, updated) => {
                  res
                    .status(200)
                    .json({ message: "Email enviado", success: true });
                }
              );
            }
          });
        }
      });
    });

    this.router.post("/tokenValidation", (req, res) => {
      userSchema.find({ forgotPasswordToken: req.body.token }, (err, data) => {
        if (err || data.length < 1) {
          if (err) console.log(err);
          res.status(200).json({ isValid: false, email: "" });
        } else if (data.length > 0) {
          res.status(200).json({ isValid: true, email: data[0].email });
        }
      });
    });

    this.router.post("/resetPassword", (req, res) => {
      var newvalues = { $unset: { forgotPasswordToken: req.body.token } };
      userSchema.findOneAndUpdate(
        { email: req.body.email },
        newvalues,
        {
          //options
          new: true,
          strict: false,
          useFindAndModify: false,
        },
        (err, updated) => {
          if (err) console.log(err);
          bcrypt.genSalt(10, (err: any, salt: any) => {
            if (err) console.log(err);

            bcrypt.hash(req.body.password, salt, (err, hash) => {
              if (err) console.log(err);
              var newvalues2 = { $set: { password: hash } };
              userSchema.findOneAndUpdate(
                { email: req.body.email },
                newvalues2,
                {
                  //options
                  new: true,
                  strict: false,
                  useFindAndModify: false,
                },
                (err, updated) => {
                  if (err) console.log(err);
                  res.status(200).json({ changed: true });
                }
              );
            });
          });
        }
      );
    });

    /// recovery restaurant

    this.router.get(
      "/forgotpassword-restaurant",
      async (req: any, res: Response) => {
        const email = req.query.email;
        await restaurantSchema.find({ email: email }, (err: any, data: any) => {
          if (data.length < 1 || err) {
            res.status(400).json({
              message: "Aún no tenemos este correo eletrónico",
              success: false,
              noEmail: true,
            });
          } else {
            const token = crypto.randomBytes(20).toString("hex");
            // write to database
            const transporter = nodemailer.createTransport({
              host: "smtp.gmail.com",
              service: "gmail",
              port: 465,
              secure: true,
              auth: {
                user: process.env.EMAIL_ADDRESS,
                pass: process.env.EMAIL_PASSWORD,
              },
            });
            const mailOptions = {
              from: process.env.EMAIL_ADDRESS,
              to: email,
              subject: "Recuperar contraseña Foody for Restaurant",
              text: "Recuperar contraseña Foody for Restaurant",
              html: recoverPasswordEmailRestaurant(token),
            };
            transporter.sendMail(mailOptions, (err: any) => {
              if (err) {
                console.log("err:", err);
              } else {
                var newvalues = { $set: { forgotPasswordToken: token } };
                restaurantSchema.findOneAndUpdate(
                  { email: email },
                  newvalues,
                  {
                    //options
                    new: true,
                    strict: false,
                    useFindAndModify: false,
                  },
                  (err, updated) => {
                    res.status(200).json({
                      message: "Email enviado éxitosamente",
                      success: true,
                    });
                  }
                );
              }
            });
          }
        });
      }
    );

    this.router.post("/tokenValidation-restaurant", (req, res) => {
      restaurantSchema.find(
        { forgotPasswordToken: req.body.token },
        (err, data) => {
          if (err || data.length < 1) {
            if (err) console.log(err);
            res.status(200).json({ isValid: false, email: "" });
          } else if (data.length > 0) {
            res.status(200).json({ isValid: true, email: data[0].email });
          }
        }
      );
    });

    this.router.post("/resetPassword-restaurant", (req, res) => {
      var newvalues = { $unset: { forgotPasswordToken: req.body.token } };
      restaurantSchema.findOneAndUpdate(
        { email: req.body.email },
        newvalues,
        {
          //options
          new: true,
          strict: false,
          useFindAndModify: false,
        },
        (err, updated) => {
          if (err) console.log(err);
          bcrypt.genSalt(10, (err: any, salt: any) => {
            if (err) console.log(err);

            bcrypt.hash(req.body.password, salt, (err, hash) => {
              if (err) console.log(err);
              var newvalues2 = { $set: { password: hash } };
              restaurantSchema.findOneAndUpdate(
                { email: req.body.email },
                newvalues2,
                {
                  //options
                  new: true,
                  strict: false,
                  useFindAndModify: false,
                },
                (err, updated) => {
                  if (err) console.log(err);
                  res.status(200).json({ changed: true });
                }
              );
            });
          });
        }
      );
    });
  }
}

const recoveryRouter = new RecoveryRouter();
recoveryRouter.routes();

export default recoveryRouter.router;
