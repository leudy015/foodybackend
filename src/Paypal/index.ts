import { Request, Response, Router } from "express";
import paypal from "paypal-rest-sdk";
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });
import orderSchema from "../models/order";
import cardSchema from "../models/card";

paypal.configure({
  mode: "live", //sandbox or live  &order=${order}
  client_id: process.env.PAYPALCLIENTID || "",
  client_secret: process.env.PAYPALCLIENTSECRET || "",
});

class PaypalRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/paypal", (req: Request, res: Response) => {
      const { price, order, cart } = req.query;
      let create_payment_json = {
        intent: "sale",
        payer: {
          payment_method: "paypal",
        },
        redirect_urls: {
          return_url: `https://api.foodyapp.es/success?price=${price}&order=${order}&cart=${cart}`,
          cancel_url: "https://api.foodyapp.es/cancel",
        },
        transactions: [
          {
            amount: {
              currency: "EUR",
              total: req.query.price,
            },
            description: "Foody Pick Up App S.L",
          },
        ],
      };
      // @ts-ignore
      paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
          console.log(error);
          throw error;
        } else {
          console.log("Create Payment Response");
          console.log(payment);
          // @ts-ignore
          res.redirect(payment.links[1].href);
        }
      });
    });

    this.router.get("/success", (req: Request, res: Response) => {
      var PayerID = req.query.PayerID;
      var paymentId = req.query.paymentId;
      const { price, order, cart } = req.query;
      var execute_payment_json = {
        payer_id: PayerID,
        transactions: [
          {
            amount: {
              currency: "EUR",
              total: price,
            },
          },
        ],
      };
      // @ts-ignore
      paypal.payment.execute(paymentId, execute_payment_json, function (
        error,
        payment
      ) {
        if (error) {
          console.log(error.response);
          throw error;
        }
        if (payment) {
          orderSchema.findOneAndUpdate(
            { _id: order },
            {
              $set: {
                estado: "Pagada",
                progreso: "25",
                status: "active",
                pagoPaypal: payment,
                total: price,
              },
            },
            (err, order) => {
              // @ts-ignore
              cardSchema.deleteMany(
                // @ts-ignore
                { restaurant: cart },
                (err: any, cart: any) => {
                  if (err) console.log(err);
                  else console.log("done update");
                }
              );
              res.render("success");
            }
          );
          console.log("Get Payment Response");
        }
      });
    });

    this.router.get("/cancel", (req: Request, res: Response) => {
      res.render("cancel");
    });
  }
}

const paypalRouter = new PaypalRouter();
paypalRouter.routes();

export default paypalRouter.router;
