import express from "express";
import morgan from "morgan";
import helmet from "helmet";
import mongoose from "mongoose";
import compression from "compression";
import cors from "cors";
import jwt from "jsonwebtoken";
import { ApolloServer } from "apollo-server-express";
import path from "path";
import bodyParser from "body-parser";

import indexRouter from "./routers/index.router";
import socialRouter from "./LoginSocial/Apis";
import recoveryRouter from "./RecoveryPassword/Recovery";
import paypalRouter from "./Paypal";
import confirmNumberRouter from "./Twilio/ConfirmNumber";
import useRauter from "./routers/userRouter";
import stripeRouter from "./stripe/stripe";
import onesignalRouter from "./OneSignal";
import constctRouter from "./routers/constact.router";
import { resolvers } from "./GraphQL/Resolvers";
import { typeDefs } from "./GraphQL/Schema";
import engines from "consolidate";

class Server {
  public app: express.Application;
  constructor() {
    this.app = express();
    this.config();
    this.router();
  }

  config() {
    const MONGO_URI = process.env.MONGO_URI || "nodata";
    mongoose.set("useFindAndModify", true);
    mongoose
      .connect(MONGO_URI, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
      })
      .then((db) => console.log("DB is conected"));
    //Settings
    this.app.set("port", process.env.PORT || 4000);
    //Middleware
    this.app.use(morgan("dev"));
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(helmet());
    this.app.use(compression());
    this.app.use(cors());
    this.app.use(function (req, res, next) {
      //to allow cross domain requests to send cookie information.
      // @ts-ignore-start
      res.setHeader("Access-Control-Allow-Credentials", true);
      // @ts-ignore-end

      // origin can not be '*' when crendentials are enabled. so need to set it to the request origin
      // @ts-nocheck
      res.setHeader("Access-Control-Allow-Origin", "*");

      // list of methods that are supported by the server
      res.setHeader(
        "Access-Control-Allow-Methods",
        "GET,PUT,POST,DELETE,OPTIONS"
      );

      res.setHeader(
        "Access-Control-Allow-Headers",
        "Authorization, Origin, X-Requested-With, Content-Type, Accept"
      );

      next();
    });
  }

  router() {
    this.app.use(indexRouter);
    this.app.use("/api/user", useRauter);
    this.app.use(socialRouter);
    this.app.use(recoveryRouter);
    this.app.use(confirmNumberRouter);
    this.app.use(paypalRouter);
    this.app.use(constctRouter);
    this.app.engine("ejs", engines.ejs);
    this.app.set("views", "views");
    this.app.set("view engine", "ejs");
    this.app.use(
      "/assets",
      express.static(path.join(__dirname + "/../uploads"))
    );
    this.app.use(stripeRouter);
    this.app.use(onesignalRouter);
    this.app.use(
      bodyParser.urlencoded({
        parameterLimit: 100000,
        limit: "50mb",
        extended: true,
      })
    );
    this.app.use(bodyParser.json({ limit: "50mb", type: "application/json" }));
  }

  start(paht: any) {
    this.app.listen(this.app.get("port"), () => {
      console.log(
        `Server on port http://localhost:${this.app.get("port")}${paht}`
      );
    });
  }
}

const servers = new ApolloServer({
  resolvers,
  typeDefs,
  context: async ({ req }) => {
    const token = req.headers["authorization"];
    if (token !== "null") {
      try {
        // verificarl el token que viene del cliente
        const usuarioActual = jwt.verify(
          token || "tokens",
          process.env.SECRETO || "mysecretToken"
        );
        req.usuarioActual = usuarioActual;

        return {
          usuarioActual,
        };
      } catch (err) {
        // console.log('err: ', err);
      }
    }
  },
});

const server = new Server();
servers.applyMiddleware(server);
server.start(servers.graphqlPath);
