import categoriaSchema from "../models/categorias";
import userSchema from "../models/user";
import restaurantSchema from "../models/restaurant";
import PlatoSchema from "../models/nemu/platos";
import MenuSchema from "../models/nemu/menu";
import pagoSchema from "../models/Pago";
import transSchema from "../models/transacciones";
import AcompananteSchema from "../models/nemu/acompanante";
import cardSchema from "../models/card";
import Cupones from "../models/cupones";
import favoritoSchema from "../models/Favorito";
import { STATUS_MESSAGES } from "./Status_messages";
import fs from "fs";
import { Types } from "mongoose";
import orderSchema from "../models/order";
import RatingSchema from "../models/rating";
import NotificationSchema from "../models/Notification";
import OpinionSchema from "../models/Opinion";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
const mailjet = require("node-mailjet").connect(
  process.env.MJ_APIKEY_PUBLIC,
  process.env.MJ_APIKEY_PRIVATE
);

const { ObjectId } = Types;

export interface IRestaurant extends Document {
  email: string;
  password: string;
}

const crearToken = (restaurant: any, secreto: any, expiresIn: any) => {
  const { _id } = restaurant;

  return jwt.sign({ _id }, secreto, { expiresIn });
};

export const Mutation = {
  autenticarRestaurant: async (root: any, { email, password }) => {
    const restaurant = await restaurantSchema.findOne({ email });
    if (!restaurant) {
      return {
        success: false,
        message: STATUS_MESSAGES.USER_NOT_FOUND,
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(
      password,
      restaurant.password
    );
    if (!passwordCorrecto) {
      return {
        success: false,
        message: STATUS_MESSAGES.INCORRECT_PASSWORD,
        data: null,
      };
    }
    return {
      success: true,
      message: "Bienvenido a Foody Restaurante",
      data: {
        token: crearToken(restaurant, process.env.SECRETO, "5000hr"),
        id: restaurant._id,
      },
    };
  },

  autenticarUsuario: async (root: any, { email, password }) => {
    const users = await userSchema.findOne({ email });
    if (!users) {
      return {
        success: false,
        message: STATUS_MESSAGES.USER_NOT_FOUND,
        data: null,
      };
    }
    const passwordCorrecto = await bcrypt.compare(password, users.password);
    if (!passwordCorrecto) {
      return {
        success: false,
        message: STATUS_MESSAGES.INCORRECT_PASSWORD,
        data: null,
      };
    }
    return {
      success: true,
      message: "Bienvenid@ a Foody",
      data: {
        token: crearToken(users, process.env.SECRETO, "5000hr"),
        id: users._id,
      },
    };
  },

  crearUsuario: async (root: any, { input }) => {
    // check if email exists
    const emailExists = await userSchema.findOne({ email: input.email });
    if (emailExists) {
      return {
        success: false,
        message: "Ya eres parte de Foody",
        data: null,
      };
    }

    const nuevoUsuario = new userSchema({
      name: input.nombre,
      lastName: input.apellidos,
      email: input.email,
      password: input.password,
      termAndConditions: input.termAndConditions,
    });

    nuevoUsuario.id = nuevoUsuario._id;

    return new Promise((resolve, object) => {
      nuevoUsuario.save((error) => {
        if (error)
          object({
            success: false,
            message: "Hubo un problema con su solicitud",
            data: null,
          });
        else {
          resolve({
            success: true,
            message: "Usuario agregado con éxito",
            data: nuevoUsuario,
          });

          const request = mailjet.post("contact").request({
            Email: input.email,
            IsExcludedFromCampaigns: "false",
            Name: input.name,
          });
          request
            .then((result: any) => {
              console.log(result.body.Data[0].ID);
              const request = mailjet
                .post("contact")
                .id(result.body.Data[0].ID)
                .action("managecontactslists")
                .request({
                  ContactsLists: [
                    {
                      ListID: 10241868,
                      Action: "addnoforce",
                    },
                  ],
                });
              request
                .then((result: any) => {
                  console.log("done");
                })
                .catch((err: any) => {
                  console.log(err.statusCode);
                });
            })
            .catch((err: any) => {
              console.log(err.statusCode);
            });
        }
      });
    });
  },

  singleUpload(parent: any, { file }) {
    const matches = file.match(/^data:.+\/(.+);base64,(.*)$/);
    const ext = matches[1];
    const base64_data = matches[2];
    const buffer = Buffer.from(base64_data, "base64");

    const filename = `${Date.now()}-file.${ext}`;
    const filenameWithPath = `${__dirname}/../../uploads/images/${filename}`;

    return new Promise((resolve, reject) => {
      fs.writeFile(filenameWithPath, buffer, (error) => {
        if (error) {
          reject(error);
          console.log(error);
        } else {
          resolve({ filename });
          console.log(filename);
        }
      });
    });
  },

  createCategory: (root: any, { input }) => {
    const nuevaCategoria = new categoriaSchema({
      title: input.title,
      image: input.image,
      description: input.description,
    });
    return new Promise((resolve, rejects) => {
      nuevaCategoria.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.USER_ADDED,
            success: true,
          });
        }
      });
    });
  },

  eliminarCategory: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      categoriaSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error) rejects(error);
        else resolve("Eliminado correctamente");
      });
    });
  },

  actualizarUsuario: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      userSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error, _usuario) => {
          if (error) reject(error);
          else resolve(_usuario);
        }
      );
    });
  },

  eliminarUsuario: (root: any, { id }) => {
    console.log(id);
    return new Promise((resolve, reject) => {
      userSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Usuario eliminado con éxito",
            success: true,
          });
      });
    });
  },

  createRestaurant: (root: any, { input }) => {
    const nuevoRestaurant = new restaurantSchema({
      title: input.title,
      image: input.image,
      description: input.description,
      rating: input.rating,
      address: {
        calle: input.address.calle,
        numero: input.address.numero,
        codigoPostal: input.address.codigoPostal,
        ciudad: input.address.ciudad,
      },
      coordetate: input.coordetate,
      coordenateone: input.coordenateone,
      categoryName: input.categoryName,
      categoryID: input.categoryID,
      minime: input.minime,
      menu: input.menu,
      phone: input.phone,
      email: input.email,
      city: input.city,
      logo: input.logo,
      password: input.password,
      type: input.type,
      apertura: input.apertura,
      cierre: input.cierre,
      diaslaborales: input.diaslaborales,
      open: input.open,
      isnew: input.isnew,
    });
    return new Promise((resolve, rejects) => {
      nuevoRestaurant.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  eliminarRestaurant: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      restaurantSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Restaurante eliminado con éxito",
            success: true,
          });
      });
    });
  },

  actualizarRestaurant: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      restaurantSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error) => {
          if (error)
            reject({
              messages:
                "Hubo un error con tu solicitud vuelve a intentalo por favor",
              success: false,
            });
          else
            resolve({
              messages: "Datos actualizado con éxito",
              success: true,
            });
        }
      );
    });
  },

  crearFavorito: async (
    root: any,
    { restaurantID, usuarioId },
    { usuarioActual }
  ) => {
    console.log(restaurantID, usuarioId);
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para comtinuar",
        success: false,
        data: null,
      };
    }

    const favorito = new favoritoSchema({
      usuarioId,
      restaurantID,
    });
    favorito.id = favorito._id;
    return new Promise((resolve, reject) => {
      favorito.save((error: any) => {
        if (error) {
          reject({
            messages: "Hay un problema con tu solicitud",
            success: false,
          });
          console.log(error);
        } else {
          resolve({
            messages: "Restaurante añadido a favorito",
            success: true,
          });
          console.log("dome favorite");
        }
      });
    });
  },

  eliminarFavorito: async (root: any, { id }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para comtinuar",
        success: false,
      };
    }
    return new Promise((resolve, reject) => {
      favoritoSchema.findOneAndDelete({ restaurantID: id }, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con tu solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Restaurante eliminado de favorito",
            success: true,
          });
      });
    });
  },

  createPlatos: (root: any, { input }) => {
    const newPlatos = new PlatoSchema({
      title: input.title,
      ingredientes: input.ingredientes,
      price: input.price,
      imagen: input.imagen,
      menu: input.menu,
      restaurant: input.restaurant,
      oferta: input.oferta,
      popular: input.popular,
      news: input.news,
    });
    return new Promise((resolve, rejects) => {
      newPlatos.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  createMenu: (root: any, { input }) => {
    const newMenu = new MenuSchema({
      title: input.title,
      subtitle: input.subtitle,
      restaurant: input.restaurant,
    });
    return new Promise((resolve, rejects) => {
      newMenu.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  createAcompanante: (root: any, { input }) => {
    const newAcompanante = new AcompananteSchema({
      name: input.name,
      children: input.children,
      plato: input.plato,
      required: input.required,
      restaurant: input.restaurant,
    });
    return new Promise((resolve, rejects) => {
      newAcompanante.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  createItemCard: async (root: any, { input }) => {
    const existenPlato = await cardSchema.findOne({
      platoID: input.platoID,
      userId: input.userId,
    });
    if (existenPlato) {
      return new Promise((resolve, reject) => {
        cardSchema.findOneAndUpdate(
          { _id: existenPlato._id },
          input,
          { new: true },
          (error, card) => {
            if (error) reject(error);
            else {
              resolve({
                messages: STATUS_MESSAGES.DATA_SUCCESS,
                success: true,
              });
            }
          }
        );
      });
    } else {
      const newCardItem = new cardSchema({
        userId: input.userId,
        restaurant: input.restaurant,
        plato: input.plato,
        platoID: input.platoID,
        total: input.total,
        extra: input.extra,
        complementos: input.complementos,
      });
      return new Promise((resolve, rejects) => {
        newCardItem.save((error: any) => {
          if (error) {
            rejects(error);
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
            });
          }
        });
      });
    }
  },

  actualizarCardItem: async (root: any, { input }) => {
    return new Promise((resolve, reject) => {
      cardSchema.findOneAndUpdate(
        { _id: input._id },
        input,
        { new: true },
        (error, card) => {
          if (error) reject(error);
          else resolve(card);
        }
      );
    });
  },

  eliminarCardItem: async (root: any, { id }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para comtinuar",
        success: false,
      };
    }
    return new Promise((resolve, reject) => {
      cardSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con tu solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Producto eliminado de tu carrito",
            success: true,
          });
      });
    });
  },

  crearCupon: async (root: any, { input }) => {
    try {
      const nuevoCupon = new Cupones({
        clave: input.clave,
        descuento: input.descuento,
        tipo: input.tipo,
      });
      return new Promise((resolve, reject) => {
        nuevoCupon.save((error, cupon) => {
          if (error) {
            console.log(
              "resolvers -> mutation -> crearCupon -> error1 ",
              error
            );
            return reject(error);
          } else {
            return resolve(cupon);
          }
        });
      });
    } catch (error) {
      console.log("resolvers -> mutation -> crearCupon -> error2 ", error);
      return new Promise((_resolve, reject) => {
        return reject({
          success: false,
          message: "Hay un problema con su solicitud",
          data: null,
        });
      });
    }
  },

  eliminarCupon: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      Cupones.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            message: "Miembro eliminado con éxito",
          });
      });
    });
  },

  crearModificarOrden: async (root: any, { input }, { usuarioActual }) => {
    try {
      if (!usuarioActual) {
        return {
          success: false,
          message: "Debes iniciar sesión para continiar",
          data: null,
        };
      }

      const usuario = await userSchema.findOne({
        _id: usuarioActual._id,
      });
      if (!usuario) {
        return {
          success: false,
          message: "Debes iniciar sesión para continiar",
          data: null,
        };
      }

      let orden: any;
      if (input.id) {
        orden = await orderSchema.findById(ObjectId(input.id)).exec();
      }

      if (!!input.restaurant) {
        const restaurants = await restaurantSchema
          .findById(ObjectId(input.restaurant))
          .exec();
        if (!restaurants) {
          return new Promise((_resolve, reject) => {
            return reject({
              success: false,
              message: "No existe un restaurante para esta orden",
              data: null,
            });
          });
        }
      }
      let cupon: any;
      if (!!input.clave) {
        cupon = await Cupones.findOne({ clave: input.clave }).exec();
      } else if (!!input.cupon) {
        cupon = ObjectId(input.cupon);
      }

      let platos: any;
      platos = await cardSchema.find({
        userId: input.userID,
        restaurant: input.restaurant,
      });

      if (!orden || !orden._id) {
        orden = new orderSchema({
          restaurant: input.restaurant,
          time: input.time,
          cupon: cupon && cupon._id ? cupon._id : cupon,
          nota: input.nota,
          aceptaTerminos: input.aceptaTerminos,
          cantidad: input.cantidad || 1,
          userID: input.userID,
          platos: platos,
          cubiertos: input.cubiertos,
          total: input.total,
          propina: input.propina,
          isvalored: input.isvalored,
        });
      } else {
        if (!orden.cupon) {
          orden.set({
            cupon: cupon && cupon._id ? cupon._id : cupon,
          });
        }
      }

      return new Promise((resolve, reject) => {
        orden.save((error: any, ordenguardada: any) => {
          if (error) {
            console.log(
              "resolvers -> mutation -> crearModificarOrden -> error1 ",
              error
            );
            return reject(error);
          } else {
            if (!ordenguardada.descuento && cupon && cupon._id) {
              ordenguardada.descuento = cupon;
            }
            return resolve(ordenguardada);
          }
        });
      });
    } catch (error) {
      console.log(
        "resolvers -> mutation -> crearModificarOrden -> error2 ",
        error
      );
      return new Promise((_resolve, reject) => {
        return reject({
          success: false,
          message: "Hay un problema con tu solicitud",
          data: null,
        });
      });
    }
  },

  crearValoracion: async (root: any, { input }) => {
    try {
      const nuevoValoracion = new RatingSchema({
        user: input.user,
        comment: input.comment,
        value: input.value,
        restaurant: input.restaurant,
      });

      return new Promise((resolve, reject) => {
        nuevoValoracion.save((error, rating) => {
          if (error) {
            console.log(
              "resolvers -> mutation -> crearCupon -> error1 ",
              error
            );
            return reject(error);
          } else {
            return resolve(rating);
          }
        });
      });
    } catch (error) {
      console.log("resolvers -> mutation -> crearCupon -> error2 ", error);
      return new Promise((_resolve, reject) => {
        return reject({
          success: false,
          message: "Hay un problema con su solicitud",
          data: null,
        });
      });
    }
  },

  createNotification: async (root: any, { input }, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para continiar",
        success: false,
      };
    }
    let newNotification = new NotificationSchema({
      user: input.user,
      usuario: input.usuario,
      restaurant: input.restaurant,
      type: input.type,
      ordenId: input.ordenId,
    });
    return new Promise((resolve, reject) => {
      newNotification.save((error) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Notificacion leída con éxito",
            success: true,
          });
      });
    });
  },

  readNotification: async (
    root: any,
    { notificationId },
    { usuarioActual }
  ) => {
    if (!usuarioActual) {
      return {
        messages: "Debes iniciar sesión para continiar",
        success: false,
      };
    }

    return new Promise((resolve, reject) => {
      NotificationSchema.findOneAndUpdate(
        { _id: ObjectId(notificationId) },
        { $set: { read: true } },
        (error) => {
          if (error)
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
            });
          else
            resolve({
              messages: "Notificacion leída con éxito",
              success: true,
            });
        }
      );
    });
  },

  createOpinion: (root: any, { input }) => {
    const nuevaOpinion = new OpinionSchema({
      plato: input.plato,
      comment: input.comment,
      rating: input.rating,
      user: input.user,
    });
    return new Promise((resolve, rejects) => {
      nuevaOpinion.save((error: any) => {
        if (error) {
          rejects(error);
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
          });
        }
      });
    });
  },

  OrdenProceed: async (root: any, { orden, estado, progreso, status }) => {
    return new Promise((resolve, reject) => {
      const message = estado === "Confirmada" ? "Confirmada" : "Rechazada";
      let dataToUpdate = { estado };
      //@ts-ignore
      if (progreso) dataToUpdate.progreso = progreso;
      //@ts-ignore
      if (status) dataToUpdate.status = status;
      orderSchema.findOneAndUpdate(
        { _id: orden },
        dataToUpdate,
        (error: any) => {
          if (error) {
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
            });
          } else {
            resolve({
              messages: "Orden procesada con éxito",
              success: true,
            });
          }
        }
      );
    });
  },

  eliminarPlato: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      PlatoSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Plato eliminado con éxito",
            success: true,
          });
      });
    });
  },

  eliminarComplemento: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      AcompananteSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Complemento eliminado con éxito",
            success: true,
          });
      });
    });
  },

  eliminarMenu: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      MenuSchema.findOneAndDelete({ _id: id }, (error: any) => {
        if (error)
          reject({
            messages: "Hubo un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Menú eliminado con éxito",
            success: true,
          });
      });
    });
  },

  crearPago: (root: any, { input }) => {
    const nuevoPago = new pagoSchema({
      nombre: input.nombre,
      iban: input.iban,
      restaurantID: input.restaurantID,
    });

    nuevoPago.id = nuevoPago._id;

    return new Promise((resolve, reject) => {
      nuevoPago.save((error) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Pago añadido con éxito",
            success: true,
          });
      });
    });
  },

  eliminarPago: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      pagoSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Pago eliminado con éxito",
            success: true,
          });
      });
    });
  },

  crearDeposito: (root: any, { input }) => {
    const nuevoDeposito = new transSchema({
      fecha: new Date(),
      estado: input.estado,
      total: input.total,
      restaurantID: input.restaurantID,
    });
    nuevoDeposito.id = nuevoDeposito._id;
    return new Promise((resolve, reject) => {
      nuevoDeposito.save((error: any) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
          });
        else
          resolve({
            messages: "Deposito añadido con éxito",
            success: true,
          });
      });
    });
  },

  eliminarDeposito: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      transSchema.findOneAndDelete({ _id: id }, (error) => {
        if (error)
          reject({
            success: false,
            messages: "Hay un problema con su solicitud",
          });
        else
          resolve({
            success: true,
            messages: "Deposito eliminado con éxito",
          });
      });
    });
  },
};
