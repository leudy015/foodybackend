import userSchema from "../models/user";
import categoriaSchema from "../models/categorias";
import restaurantSchema from "../models/restaurant";
import MenuSchema from "../models/nemu/menu";
import AcompananteSchema from "../models/nemu/acompanante";
import favoritoSchema from "../models/Favorito";
import Cupones from "../models/cupones";
import cardSchema from "../models/card";
import orderSchema from "../models/order";
import { STATUS_MESSAGES } from "./Status_messages";
import RatingSchema from "../models/rating";
import NotificationSchema from "../models/Notification";
import OpinionSchema from "../models/Opinion";
import pagoSchema from "../models/Pago";
import transSchema from "../models/transacciones";

export const Query = {
  getUsuario: (root: any, {}, { usuarioActual }) => {
    if (!usuarioActual) {
      return {
        success: false,
        messages: STATUS_MESSAGES.NOT_LOGGED_IN,
        data: {},
      };
    } else {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: usuarioActual._id }, (err, res) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: {},
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        });
      });
    }
  },

  obtenerUsuario: (root: any, args: any, { usuarioActual }) => {
    if (!usuarioActual) {
      return null;
    }
    const usuario = userSchema.findOne({ _id: usuarioActual._id });

    return usuario;
  },

  getCategory: (root: any) => {
    return new Promise((resolve, rejects) => {
      categoriaSchema.find((err, res) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getRestaurant: (root: any, { city }) => {
    return new Promise((resolve, rejects) => {
      restaurantSchema.find({ city: city }, (err: any, res: any) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getRestaurantSearch: (root: any, { city, price, category, search }) => {
    let condition = {};

    if (search) condition = { $text: { $search: `"\"${search} \""` } };
    // @ts-ignore
    if (price) condition.minime = { $lt: price };
    // @ts-ignore
    if (city) condition.city = city;
    // @ts-ignore
    if (category) condition.categoryID = category;

    return new Promise((resolve, rejects) => {
      restaurantSchema.find(
        condition,
        { score: { $meta: "textScore" } },
        (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        }
      );
    });
  },

  getRestaurantForCategory: (root: any, { city, category }) => {
    return new Promise((resolve, rejects) => {
      restaurantSchema.find(
        { city: city, categoryID: category },
        (err: any, res: any) => {
          if (err) {
            rejects({
              messages: STATUS_MESSAGES.S_WENT_WRONG,
              success: false,
              data: [],
            });
          } else {
            resolve({
              messages: STATUS_MESSAGES.DATA_SUCCESS,
              success: true,
              data: res,
            });
          }
        }
      );
    });
  },

  getRestaurantForID: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      restaurantSchema.findOne({ _id: id }, (err: any, res: any) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: {},
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      });
    });
  },

  getRestaurantFavorito: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      favoritoSchema.find({ usuarioId: id }, (error: any, favourite: any) => {
        if (error) {
          console.log("error ==>: ", error);

          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: favourite,
          });
        }
      });
    });
  },

  getMenu: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      MenuSchema.find({ restaurant: id }, (error: any, favourite: any) => {
        if (error) {
          console.log("error ==>: ", error);

          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: favourite,
          });
        }
      }).sort({ $natural: -1 });
    });
  },

  getAcompanante: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      AcompananteSchema.find({ plato: id }, (error: any, favourite: any) => {
        if (error) {
          console.log("error ==>: ", error);
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: favourite,
          });
        }
      });
    });
  },

  getItemCart: (root: any, { id, PlatoID }) => {
    return new Promise((resolve, reject) => {
      cardSchema.find(
        { userId: id, platoID: PlatoID },
        (error: any, cart: any) => {
          if (error) {
            console.log("error ==>: ", error);

            reject({
              success: false,
              message: "Hay un problema con su solicitud",
              list: [],
            });
          } else {
            resolve({
              success: true,
              message: "Operación realizada con éxito",
              list: cart,
            });
          }
        }
      );
    });
  },

  getItemCarts: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      cardSchema.find({ userId: id }, (error: any, cart: any) => {
        if (error) {
          console.log("error ==>: ", error);
          reject({
            success: false,
            message: "Hay un problema con su solicitud",
            list: [],
          });
        } else {
          resolve({
            success: true,
            message: "Operación realizada con éxito",
            list: cart,
          });
        }
      });
    });
  },

  getMyItemCart: (root: any, { id, restaurant }) => {
    return new Promise((resolve, reject) => {
      cardSchema.find(
        { userId: id, restaurant: restaurant },
        (error: any, cart: any) => {
          if (error) {
            console.log("error ==>: ", error);

            reject({
              success: false,
              message: "Hay un problema con su solicitud",
              list: [],
            });
          } else {
            resolve({
              success: true,
              message: "Operación realizada con éxito",
              list: cart,
            });
          }
        }
      );
    });
  },

  getCupon: async (root: any, { clave }, { usuarioActual }) => {
    try {
      if (!usuarioActual) {
        return {
          success: false,
          message: "Debe iniciar sesión para continuar",
          data: null,
        };
      }
      const usuario = await userSchema.findOne({
        _id: usuarioActual._id,
      });
      if (!usuario) {
        return {
          success: false,
          message: "Debe iniciar sesión para continuar",
          data: null,
        };
      }
      return new Promise((resolve, reject) => {
        Cupones.findOne({ clave }, (error, cupon) => {
          if (error) {
            console.log("resolvers -> query -> getCupon -> error1 ", error);
            return reject(error);
          } else {
            return resolve(cupon);
          }
        });
      });
    } catch (error) {
      console.log("resolvers -> query -> getCupon -> error2 ", error);
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getCuponAll: async (root: any) => {
    try {
      return new Promise((resolve, reject) => {
        Cupones.findOne((error: any, cupon: any) => {
          if (error) {
            console.log("resolvers -> query -> getCupon -> error1 ", error);
            return reject(error);
          } else {
            return resolve(cupon);
          }
        });
      });
    } catch (error) {
      console.log("resolvers -> query -> getCupon -> error2 ", error);
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getOrderByUsuario: async (
    root: any,
    { usuarios, dateRange },
    { usuarioActual }
  ) => {
    if (!usuarioActual) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    const usuario = await userSchema.findOne({
      _id: usuarioActual._id,
    });
    if (!usuario) {
      return {
        success: false,
        message: "Debe iniciar sesión para continuar",
        list: [],
      };
    }

    return new Promise((resolve, reject) => {
      let condition = {
        userID: usuarios,
        estado: { $ne: "Pendiente de pago" },
      };
      //let condition = { cliente: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      orderSchema
        .find(condition, (error: any, consulta: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: consulta,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getOrderByRestaurant: async (root: any, { id, dateRange }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        restaurant: id,
        estado: { $ne: "Pendiente de pago" },
      };
      //let condition = { cliente: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      orderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: order,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getOrderByRestaurantProcess: async (root: any, { id, dateRange }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        restaurant: id,
        estado: {
          $in: ["Preparando", "Confirmada"],
        },
      };
      //let condition = { cliente: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      orderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: order,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getOrderByRestaurantListos: async (root: any, { id, dateRange }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        restaurant: id,
        estado: {
          $in: ["Lista para recojer"],
        },
      };
      //let condition = { cliente: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      orderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: order,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getOrderByRestaurantNew: async (root: any, { id, dateRange }) => {
    return new Promise((resolve, reject) => {
      let condition = {
        restaurant: id,
        estado: {
          $in: ["Pagada"],
        },
      };
      //let condition = { cliente: "5d8556514c10b81c6a65295a" };
      if (dateRange && dateRange.fromDate && dateRange.toDate) {
        // @ts-ignore
        condition.created_at = {
          $gte: dateRange.fromDate,
          $lte: dateRange.toDate,
        };
      }
      orderSchema
        .find(condition, (error: any, order: any) => {
          if (error)
            reject({
              success: false,
              message: "Hubo un problema con su solicitud",
              list: [],
            });
          else {
            resolve({
              success: true,
              message: "Consulta extraida con éxito",
              list: order,
            });
          }
        })
        .sort({ $natural: -1 });
    });
  },

  getOrderByRestaurantID: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      orderSchema.findOne({ _id: id }, (error: any, order: any) => {
        if (error)
          reject({
            success: false,
            message: "Hubo un problema con su solicitud",
            list: [],
          });
        else {
          resolve({
            success: true,
            message: "Consulta extraida con éxito",
            list: order,
          });
        }
      });
    });
  },

  getValoraciones: async (root: any, { restaurant }) => {
    try {
      return new Promise((resolve, reject) => {
        RatingSchema.find(
          { restaurant: restaurant },
          (error: any, valoracion: any) => {
            if (error) {
              console.log("resolvers -> query -> getCupon -> error1 ", error);
              return reject(error);
            } else {
              resolve({
                messages: "Datos Obtenidos con éxito",
                success: true,
                data: valoracion,
              });
            }
          }
        ).sort({ $natural: -1 });
      });
    } catch (error) {
      console.log("resolvers -> query -> getCupon -> error2 ", error);
      return {
        success: false,
        message: "Hay un problema con su solicitud",
        data: null,
      };
    }
  },

  getNotifications: (root: any, { Id }) => {
    if (Id) {
      return new Promise((resolve, reject) => {
        NotificationSchema.find({ user: Id, read: false })
          .populate("users")
          .populate("restaurants")
          .populate("orders")
          .sort({ $natural: -1 })
          .exec((error, notification) => {
            if (error) {
              console.log(error);
              reject({
                messages: "Hubo un problema con su solicitud",
                success: false,
                notifications: [],
              });
            } else {
              resolve({
                messages: "success",
                success: true,
                notifications: notification,
              });
            }
          });
      });
    } else {
      return null;
    }
  },

  getOpinion: (root: any, { id }) => {
    return new Promise((resolve, rejects) => {
      OpinionSchema.find({ plato: id }, (err, res) => {
        if (err) {
          rejects({
            messages: STATUS_MESSAGES.S_WENT_WRONG,
            success: false,
            data: [],
          });
        } else {
          resolve({
            messages: STATUS_MESSAGES.DATA_SUCCESS,
            success: true,
            data: res,
          });
        }
      }).sort({ $natural: -1 });
    });
  },

  getPago: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      pagoSchema.findOne({ restaurantID: id }, (error: any, pagos: any) => {
        if (error)
          reject({
            messages: "Hay un problema con su solicitud",
            success: false,
            data: null,
          });
        else
          resolve({
            messages: "Operacion realizada con éxito",
            success: true,
            data: pagos,
          });
      });
    });
  },

  getTransaction: async (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      transSchema
        .find({ restaurantID: id })
        .sort({ $natural: -1 })
        .exec((error, deposito) => {
          if (error)
            reject({
              messages: "Hay un problema con su solicitud",
              success: false,
              list: [],
            });
          else
            resolve({
              messages: "solicitud procesada con éxito",
              success: true,
              list: deposito,
            });
        });
    });
  },

  getStatistics: (root: any, { id }) => {
    return new Promise((resolve, reject) => {
      let matchQuery1 = {
        restaurant: id,
        estado: "Recogido",
      };

      let matchQuery2 = {
        restaurant: id,
        estado: "Rechazada",
      };

      orderSchema
        .aggregate([
          { $match: matchQuery1 },
          {
            $group: {
              _id: {
                month: { $substr: ["$created_at", 5, 2] },
                estado: "Recogido",
              },
              stripePaymentIntent: { $first: "$stripePaymentIntent" },
              pagoPaypal: { $first: "$pagoPaypal" },
              paypalAmount: { $sum: "$pagoPaypal.montoPagado" },
              stripeAmount: { $sum: "$stripePaymentIntent.amount" },
              finishedCount: { $sum: 1 },
              created_at: { $first: "$created_at" },
            },
          },
          {
            $project: {
              your_year_variable: { $year: "$created_at" },
              paypalAmount: 1,
              finishedCount: 1,
              stripeAmountDivide: { $divide: ["$stripeAmount", 100] },
            },
          },
          { $match: { your_year_variable: 2020 } },
        ])
        .then((res) => {
          let ordenes = {
            name: "Pedidos",
            Ene: 0,
            Feb: 0,
            Mar: 0,
            Abr: 0,
            May: 0,
            Jun: 0,
            Jul: 0,
            Aug: 0,
            Sep: 0,
            Oct: 0,
            Nov: 0,
            Dic: 0,
          };
          let ganacias = {
            name: "Total Ventas",
            Ene: 0,
            Feb: 0,
            Mar: 0,
            Abr: 0,
            May: 0,
            Jun: 0,
            Jul: 0,
            Aug: 0,
            Sep: 0,
            Oct: 0,
            Nov: 0,
            Dic: 0,
          };
          let devoluciones = {
            name: "Pedidos rechazados",
            Ene: 0,
            Feb: 0,
            Mar: 0,
            Abr: 0,
            May: 0,
            Jun: 0,
            Jul: 0,
            Aug: 0,
            Sep: 0,
            Oct: 0,
            Nov: 0,
            Dic: 0,
          };

          for (let i = 0; i < res.length; i++) {
            let month = res[i]._id.month;
            if (month == "01") {
              ganacias["Ene"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Ene"] = res[i].finishedCount;
            } else if (month == "02") {
              ganacias["Feb"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Feb"] = res[i].finishedCount;
            } else if (month == "03") {
              ganacias["Mar"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Mar"] = res[i].finishedCount;
            } else if (month == "04") {
              ganacias["Abr"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Abr"] = res[i].finishedCount;
            } else if (month == "05") {
              ganacias["May"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["May"] = res[i].finishedCount;
            } else if (month == "06") {
              ganacias["Jun"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Jun"] = res[i].finishedCount;
            } else if (month == "07") {
              ganacias["Jul"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Jul"] = res[i].finishedCount;
            } else if (month == "08") {
              ganacias["Aug"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Aug"] = res[i].finishedCount;
            } else if (month == "09") {
              ganacias["Sep"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Sep"] = res[i].finishedCount;
            } else if (month == "10") {
              ganacias["Oct"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Oct"] = res[i].finishedCount;
            } else if (month == "11") {
              ganacias["Nov"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Nov"] = res[i].finishedCount;
            } else if (month == "12") {
              ganacias["Dic"] = Math.round(
                (isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                  (isNaN(res[i].stripeAmountDivide)
                    ? 0
                    : res[i].stripeAmountDivide)
              );
              ordenes["Dic"] = res[i].finishedCount;
            }
          }

          orderSchema
            .aggregate([
              { $match: matchQuery2 },
              {
                $group: {
                  _id: {
                    month: { $substr: ["$created_at", 5, 2] },
                    estado: "Rechazada",
                  },

                  returnedCount: { $sum: 1 },
                  created_at: { $first: "$created_at" },
                },
              },
              {
                $project: {
                  your_year_variable: { $year: "$created_at" },
                  returnedCount: 1,
                },
              },
              { $match: { your_year_variable: 2020 } },
            ])
            .then((res1) => {
              for (let i = 0; i < res1.length; i++) {
                let month = res1[i]._id.month;
                if (month == "01") {
                  devoluciones["Ene"] = res1[i].returnedCount;
                } else if (month == "02") {
                  devoluciones["Feb"] = res1[i].returnedCount;
                } else if (month == "03") {
                  devoluciones["Mar"] = res1[i].returnedCount;
                } else if (month == "04") {
                  devoluciones["Abr"] = res1[i].returnedCount;
                } else if (month == "05") {
                  devoluciones["May"] = res1[i].returnedCount;
                } else if (month == "06") {
                  devoluciones["Jun"] = res1[i].returnedCount;
                } else if (month == "07") {
                  devoluciones["Jul"] = res1[i].returnedCount;
                } else if (month == "08") {
                  devoluciones["Aug"] = res1[i].returnedCount;
                } else if (month == "09") {
                  devoluciones["Sep"] = res1[i].returnedCount;
                } else if (month == "10") {
                  devoluciones["Oct"] = res1[i].returnedCount;
                } else if (month == "11") {
                  devoluciones["Nov"] = res1[i].returnedCount;
                } else if (month == "12") {
                  devoluciones["Dic"] = res1[i].returnedCount;
                }
              }
              resolve({
                success: true,
                message: "",
                data: [ordenes, ganacias, devoluciones],
              });
            });
        })
        .catch((err) => {
          console.log(err);
        });
    });
  },
};
