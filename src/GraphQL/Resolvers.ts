import { Query } from "./query";
import { Mutation } from "./mutation";
import userSchema from "../models/user";
import favoritoSchema from "../models/Favorito";
import restaurantSchema from "../models/restaurant";
import PlatoSchema from "../models/nemu/platos";
import RatingSchema from "../models/rating";
import orderSchema from "../models/order";
import OpinionSchema from "../models/Opinion";

export const resolvers = {
  Query,
  Mutation,

  Platos: {
    opiniones(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        OpinionSchema.find({ plato: parent._id }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },
  },

  Opinion: {
    Usuario(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.user }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },
  },

  Notification: {
    Usuarios(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.usuario }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },

    Restaurant(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        restaurantSchema.findOne(
          { _id: parent.restaurant },
          (error, restaurant) => {
            if (error) rejects(error);
            else {
              if (!restaurant) resolve(false);
              else {
                resolve(restaurant);
              }
            }
          }
        );
      });
    },

    Orden(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        orderSchema.findOne({ _id: parent.ordenId }, (error, orden) => {
          if (error) rejects(error);
          else {
            if (!orden) resolve(false);
            else {
              resolve(orden);
            }
          }
        });
      });
    },
  },

  Valoracion: {
    Usuario(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.user }, (error, user) => {
          if (error) rejects(error);
          else {
            if (!user) resolve(false);
            else {
              resolve(user);
            }
          }
        });
      });
    },
  },

  Orden: {
    restaurants(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        restaurantSchema.findOne(
          { _id: parent.restaurant },
          (error, restaurant) => {
            if (error) rejects(error);
            else {
              if (!restaurant) resolve(false);
              else {
                resolve(restaurant);
              }
            }
          }
        );
      });
    },
    usuario(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        userSchema.findOne({ _id: parent.userID }, (error, userID) => {
          if (error) rejects(error);
          else {
            if (!userID) resolve(false);
            else {
              resolve(userID);
            }
          }
        });
      });
    },
  },

  Menu: {
    platos(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        PlatoSchema.find({ menu: parent._id }, (error, plato) => {
          if (error) rejects(error);
          else {
            if (!plato) resolve(false);
            else {
              resolve(plato);
            }
          }
        }).sort({ $natural: -1 });
      });
    },
  },

  Restaurant: {
    anadidoFavorito(parent: any, args: any, { usuarioActual }) {
      return new Promise((resolve, reject) => {
        if (!usuarioActual) resolve(false);
        userSchema.findOne({ _id: usuarioActual._id }, (error, usuario) => {
          if (error) reject(error);
          else {
            if (!usuario) resolve(false);
            else {
              favoritoSchema.findOne(
                { usuarioId: usuario._id, restaurantID: parent._id },
                (error, favorito) => {
                  if (error) reject(error);
                  else {
                    if (!favorito) resolve(false);
                    else resolve(true);
                  }
                }
              );
            }
          }
        });
      });
    },

    Valoracion(parent: any, args: any) {
      return new Promise((resolve, rejects) => {
        RatingSchema.find({ restaurant: parent._id }, (error, valoracion) => {
          if (error) rejects(error);
          else {
            if (!valoracion) resolve(false);
            else {
              resolve(valoracion);
            }
          }
        });
      });
    },
  },

  RestaurantFavorito: {
    restaurant(parent: any) {
      return new Promise((resolve, reject) => {
        restaurantSchema.findById(
          { _id: parent.restaurantID },
          (error, restaurant) => {
            if (error) {
              console.log("erro in chiild: ", error);
              reject(error);
            } else {
              resolve(restaurant);
            }
          }
        );
      });
    },
  },

  Cart: {
    Restaurant(parent: any) {
      return new Promise((resolve, reject) => {
        restaurantSchema.findById(
          { _id: parent.restaurant },
          (error, restaurant) => {
            if (error) {
              console.log("erro in chiild: ", error);
              reject(error);
            } else {
              resolve(restaurant);
            }
          }
        );
      });
    },

    UserId(parent: any) {
      return new Promise((resolve, reject) => {
        userSchema.findById({ _id: parent.userId }, (error, user) => {
          if (error) {
            console.log("erro in chiild: ", error);
            reject(error);
          } else {
            resolve(user);
          }
        });
      });
    },
  },
};
