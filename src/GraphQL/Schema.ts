import { gql } from "apollo-server-express";

export const typeDefs = gql`
  scalar Date

  type CrearUsuarioResponse {
    success: Boolean!
    message: String
    data: Usuario
  }

  type getStatisticsResponse {
    success: Boolean!
    message: String
    data: [Statistics]
  }

  type PagoResponseList {
    messages: String
    success: Boolean!
    data: Pago
  }

  type transitionResponselist {
    messages: String
    success: Boolean!
    list: [Transacion]
  }

  type Transacion {
    id: ID
    fecha: String
    estado: String
    total: String
    restaurantID: String
    created_at: String
  }

  type generarResponse {
    messages: String!
    success: Boolean!
  }

  type AutenticarRestaurantResponse {
    success: Boolean!
    message: String
    data: AutenticarRestaurent
  }

  type AutenticarRestaurent {
    token: String!
    id: String!
  }

  type AutenticarusuarioResponse {
    success: Boolean!
    message: String
    data: AutenticarUsuario
  }

  type AutenticarUsuario {
    token: String!
    id: String!
    verifyPhone: Boolean
  }

  type OrdenListResponse {
    success: Boolean!
    message: String
    list: [Orden]
  }

  type Pago {
    _id: String
    nombre: String
    iban: String
    restaurantID: String
  }

  type Address {
    calle: String
    numero: String
    codigoPostal: String
    ciudad: String
  }

  type restaurantResponse {
    success: Boolean!
    message: String
    list: [RestaurantFavorito]
  }

  type ItemCartResponse {
    success: Boolean!
    message: String
    list: [Cart]
  }

  type getNotificationResponse {
    messages: String
    success: Boolean
    notifications: [Notification]
  }

  type Notification {
    _id: String
    user: ID
    usuario: String
    read: Boolean
    ordenId: ID
    restaurant: String
    type: String
    created_at: String
    Usuarios: Usuario
    Restaurant: Restaurant
    Orden: Orden
  }

  type AcompananteResponse {
    success: Boolean!
    message: String
    list: [Acompanante]
  }

  type Acompanante {
    id: String
    name: String
    children: [Children]
    restaurant: String
    required: Boolean
    plato: String
  }

  type Children {
    id: Int
    name: String
    price: String
    cant: Int
  }

  type menuResponse {
    success: Boolean!
    message: String
    list: [Menu]
  }

  type OrderListResponse {
    success: Boolean
    message: String
    list: [Orden]
  }

  type OrderListResponseID {
    success: Boolean
    message: String
    list: Orden
  }

  type ValoracionResponse {
    messages: String
    success: Boolean
    data: [Valoracion]
  }

  type OpinionResponse {
    messages: String
    success: Boolean
    data: [Opinion]
  }

  type Cupon {
    id: ID
    clave: String
    descuento: Float
    tipo: String
  }

  type Menu {
    _id: String
    title: String
    subtitle: String
    restaurant: String
    platos: [Platos]
  }

  type PlatoOrder {
    userId: String
    platoID: String
    restaurant: String
    complementos: [String]
    plato: Platos
  }

  type Statistics {
    name: String
    Ene: Int
    Feb: Int
    Mar: Int
    Abr: Int
    May: Int
    Jun: Int
    Jul: Int
    Aug: Int
    Sep: Int
    Oct: Int
    Nov: Int
    Dic: Int
  }

  type Orden {
    id: ID
    cupon: ID
    nota: String
    aceptaTerminos: Boolean
    restaurant: String
    restaurants: Restaurant
    time: String
    cantidad: Int
    userID: ID
    usuario: Usuario
    descuento: Cupon
    propina: Boolean
    platos: [PlatoOrder]
    cubiertos: Boolean
    estado: String
    status: String
    progreso: String
    created_at: Date
    total: String
    isvalored: Boolean
  }

  type Cart {
    id: String
    userId: String
    platoID: String
    restaurant: String
    complementos: [String]
    Restaurant: Restaurant
    plato: Platos
    total: String
    extra: String
    UserId: Usuario
  }

  type Platos {
    _id: String
    title: String
    ingredientes: String
    price: String
    imagen: String
    restaurant: String
    menu: String
    oferta: Boolean
    popular: Boolean
    anadidoCart: Boolean
    cant: Int
    opiniones: [Opinion]
    news: Boolean
  }

  type RestaurantFavorito {
    _id: String
    restaurantID: String
    usuarioId: String
    restaurant: Restaurant
  }

  type Opinion {
    id: String
    plato: String
    comment: String
    rating: Int
    created_at: Date
    user: String
    Usuario: Usuario
  }

  type Valoracion {
    id: String
    user: String
    comment: String
    value: Int
    created_at: Date
    restaurant: String
    Usuario: Usuario
  }

  type Restaurant {
    _id: String
    title: String
    image: String
    description: String
    type: String
    rating: String
    address: Address
    coordetate: String
    anadidoFavorito: Boolean
    coordenateone: String
    categoryName: String
    categoryID: String
    minime: Int
    menu: String
    Valoracion: [Valoracion]
    city: String
    phone: String
    email: String
    logo: String
    password: String
    apertura: String
    cierre: String
    diaslaborales: [String]
    isnew: Boolean
    open: Boolean
    OnesignalID: String
  }

  type Usuario {
    _id: String
    name: String
    lastName: String
    email: String
    city: String
    avatar: String
    telefono: String
    created_at: Date
    updated_at: Date
    termAndConditions: Boolean
    verifyPhone: Boolean
    StripeID: String
    OnesignalID: String
  }

  type UsuarioResponse {
    messages: String!
    success: Boolean!
    data: Usuario
  }

  type RestaurantResponse {
    messages: String!
    success: Boolean!
    data: [Restaurant]
  }

  type RestaurantIDResponse {
    messages: String!
    success: Boolean!
    data: Restaurant
  }

  type CategoryResponse {
    messages: String!
    success: Boolean!
    data: [Category]
  }

  type Category {
    _id: String
    title: String
    image: String
    description: String
  }

  type File {
    filename: String
    mimetype: String
    encoding: String
  }

  type Card {
    id: String
    userId: String
    restaurant: String
    plato: String
    complementos: [String]
  }

  input CategoryInput {
    title: String
    image: String
    description: String
  }

  input Addressinput {
    calle: String
    numero: String
    codigoPostal: String
    ciudad: String
  }

  input RestaurantInput {
    title: String
    image: String
    description: String
    rating: String
    address: Addressinput
    coordetate: String
    coordenateone: String
    categoryName: String
    categoryID: String
    minime: Int
    city: String
    menu: String
    phone: String
    email: String
    logo: String
    password: String
    type: String
    apertura: String
    cierre: String
    diaslaborales: [String]
    open: Boolean
    isnew: Boolean
  }

  input ActualizarRestaurantInput {
    _id: String
    title: String
    image: String
    description: String
    address: Addressinput
    coordetate: String
    coordenateone: String
    categoryName: String
    categoryID: String
    minime: Int
    city: String
    menu: String
    phone: String
    email: String
    logo: String
    password: String
    type: String
    apertura: String
    cierre: String
    isnew: Boolean
    diaslaborales: [String]
    open: Boolean
  }

  input ActualizarUsuarioInput {
    _id: String
    name: String
    lastName: String
    city: String
    email: String
    avatar: String
  }

  input FileInput {
    filename: String
    mimetype: String
    encoding: String
  }

  input CreatePlatoInput {
    title: String
    ingredientes: String
    price: String
    restaurant: String
    imagen: String
    menu: String
    oferta: Boolean
    popular: Boolean
    news: Boolean
  }

  input CreateMenuInput {
    title: String
    subtitle: String
    restaurant: String
  }

  input CreateAcompananteInput {
    name: String
    children: [Childrens]
    restaurant: String
    required: Boolean
    plato: String
  }

  input Childrens {
    id: Int
    name: String
    price: String
    cant: Int
  }

  input PlatosCardInput {
    _id: String
    title: String
    ingredientes: String
    price: String
    imagen: String
    restaurant: String
    menu: String
    oferta: Boolean
    popular: Boolean
    cant: Int
    anadidoCart: Boolean
  }

  input CreateCardInput {
    userId: String
    restaurant: String
    plato: PlatosCardInput
    platoID: String
    total: String
    extra: String
    complementos: [String]
  }

  input AtualizarCardInput {
    id: String
    userId: String
    restaurant: String
    platoID: String
    plato: PlatosCardInput
    total: String
    extra: String
    complementos: [String]
  }

  input CuponCreationInput {
    clave: String!
    descuento: Int
    tipo: String
  }

  input ValoracionCreationInput {
    user: String
    comment: String
    value: Int
    restaurant: String
  }

  input OrdenCreationInput {
    id: ID
    cupon: ID
    nota: String
    aceptaTerminos: Boolean
    restaurant: String
    time: String
    cantidad: Int
    clave: String
    userID: ID
    propina: Boolean
    cubiertos: Boolean
    estado: String
    status: String
    progreso: String
    created_at: Date
    total: String
    isvalored: Boolean
  }

  input DateRangeInput {
    fromDate: String
    toDate: String
  }

  input NotificationInput {
    user: String
    usuario: String
    ordenId: String
    restaurant: String
    type: String
  }

  input OpinionInput {
    plato: String
    comment: String
    rating: Int
    user: String
  }

  input PagoInput {
    nombre: String
    iban: String
    restaurantID: String
  }

  input trasitionInput {
    fecha: String
    estado: String
    total: String
    restaurantID: String
  }

  input UsuarioInput {
    nombre: String
    apellidos: String
    email: String
    password: String
    termAndConditions: Boolean
  }

  type Query {
    getUsuario: UsuarioResponse
    getCategory: CategoryResponse
    getRestaurant(city: String): RestaurantResponse
    getRestaurantSearch(
      city: String
      price: Int
      category: String
      search: String
    ): RestaurantResponse
    getRestaurantForCategory(city: String, category: String): RestaurantResponse
    getRestaurantForID(id: ID): RestaurantIDResponse
    getRestaurantFavorito(id: ID): restaurantResponse
    getMenu(id: ID): menuResponse
    getAcompanante(id: ID): AcompananteResponse
    getItemCart(id: ID, PlatoID: ID): ItemCartResponse
    getItemCarts(id: ID): ItemCartResponse
    getMyItemCart(id: ID, restaurant: ID): ItemCartResponse
    getCupon(clave: String): Cupon
    getCuponAll: Cupon
    getOrderByUsuario(
      usuarios: ID
      dateRange: DateRangeInput
    ): OrderListResponse

    getOrderByRestaurant(id: ID, dateRange: DateRangeInput): OrderListResponse
    getOrderByRestaurantProcess(
      id: ID
      dateRange: DateRangeInput
    ): OrderListResponse
    getOrderByRestaurantListos(
      id: ID
      dateRange: DateRangeInput
    ): OrderListResponse

    getOrderByRestaurantNew(
      id: ID
      dateRange: DateRangeInput
    ): OrderListResponse

    getOrderByRestaurantID(id: ID): OrderListResponseID
    getValoraciones(restaurant: ID): ValoracionResponse
    getNotifications(Id: ID): getNotificationResponse
    getOpinion(id: ID): OpinionResponse
    getPago(id: ID): PagoResponseList
    getTransaction(id: ID): transitionResponselist
    getStatistics(id: ID): getStatisticsResponse
    obtenerUsuario: Usuario
  }

  type Mutation {
    createCategory(input: CategoryInput): generarResponse
    createRestaurant(input: RestaurantInput): generarResponse
    eliminarRestaurant: generarResponse
    eliminarCategory: generarResponse
    actualizarRestaurant(input: ActualizarRestaurantInput): generarResponse
    actualizarUsuario(input: ActualizarUsuarioInput): Usuario
    singleUpload(file: Upload): File
    eliminarUsuario(id: ID!): generarResponse
    crearFavorito(restaurantID: ID, usuarioId: ID): generarResponse
    eliminarFavorito(id: ID): generarResponse
    createPlatos(input: CreatePlatoInput): generarResponse
    createMenu(input: CreateMenuInput): generarResponse
    createAcompanante(input: CreateAcompananteInput): generarResponse
    createItemCard(input: CreateCardInput): generarResponse
    actualizarCardItem(input: AtualizarCardInput): Card
    eliminarCardItem(id: ID): generarResponse
    crearCupon(input: CuponCreationInput): Cupon
    eliminarCupon(id: ID): generarResponse
    crearModificarOrden(input: OrdenCreationInput): Orden
    crearValoracion(input: ValoracionCreationInput): Valoracion
    readNotification(notificationId: ID): generarResponse
    createNotification(input: NotificationInput): generarResponse
    createOpinion(input: OpinionInput): generarResponse
    autenticarRestaurant(
      email: String
      password: String
    ): AutenticarRestaurantResponse

    autenticarUsuario(
      email: String
      password: String
    ): AutenticarusuarioResponse

    OrdenProceed(
      orden: ID
      estado: String
      progreso: String
      status: String
    ): generarResponse
    eliminarPlato(id: ID): generarResponse
    eliminarComplemento(id: ID): generarResponse
    eliminarMenu(id: ID): generarResponse
    crearPago(input: PagoInput): generarResponse
    eliminarPago(id: ID): generarResponse
    crearDeposito(input: trasitionInput): generarResponse
    eliminarDeposito(id: ID): generarResponse
    crearUsuario(input: UsuarioInput): CrearUsuarioResponse
  }
`;
