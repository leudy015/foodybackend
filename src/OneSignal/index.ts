import { Request, Response, Router } from "express";
import userSchema from "../models/user";
import restaurantSchema from "../models/restaurant";
var Pushy = require("pushy");

class OnesignalRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get(
      "/save-userid-notification",
      (req: Request, res: Response) => {
        const { OnesignalID, id } = req.query;
        if (OnesignalID && id) {
          userSchema
            .findOneAndUpdate(
              { _id: id },
              { $set: { OnesignalID: OnesignalID } },
              (err: any, user: any) => {}
            )
            .catch((err: any) => {
              console.log(err);
            });
        }
      }
    );

    this.router.get("/save-restaurant-notification", (req: any, res: any) => {
      const { OnesignalID, id } = req.query;
      if (OnesignalID && id) {
        restaurantSchema
          .findOneAndUpdate(
            { _id: id },
            { $set: { OnesignalID: OnesignalID } },
            (err, user) => {}
          )
          .catch((err) => {
            console.log(err);
          });
      }
    });

    this.router.get(
      "/send-push-notification",
      (req: Request, res: Response) => {
        const { IdOnesignal, textmessage } = req.query;
        var sendNotification = function (data: any) {
          var headers = {
            "Content-Type": "application/json; charset=utf-8",
          };
          var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers,
          };
          var https = require("https");
          var req = https.request(options, function (res: any) {
            res.on("data", function (data: any) {
              console.log("Response:");
              console.log(JSON.parse(data));
            });
          });

          req.on("error", function (e: any) {
            console.log("ERROR:");
            console.log(e);
          });

          req.write(JSON.stringify(data));
          req.end();
        };

        var message = {
          app_id: "db79b975-e551-4741-ae43-8968ceab5f09",
          headings: { en: "Foody App" },
          contents: { en: `${textmessage}` },
          ios_badgeCount: 1,
          ios_badgeType: "Increase",
          include_player_ids: [`${IdOnesignal}`],
        };
        sendNotification(message);
      }
    );

    this.router.get(
      "/send-push-notification-restaurant",
      (req: Request, res: Response) => {
        const { IdOnesignal, textmessage } = req.query;
        // Plug in your Secret API Key
        // Get it here: https://dashboard.pushy.me/
        var pushyAPI = new Pushy(
          "76b0ec4e467bd35c5d857c85949f48cf8951d98a6ccd9b94a0c7d894dfcecb6b"
        );

        // Set push payload data to deliver to device(s)
        var data = {
          message: textmessage,
        };

        // Insert target device token(s) here
        var to = [IdOnesignal];

        // Optionally, send to a publish/subscribe topic instead
        // to = '/topics/news';

        // Set optional push notification options (such as iOS notification fields)
        var options = {
          notification: {
            badge: 1,
            sound: "ping.aiff",
            body: "Nuevo pedido \u270c",
          },
        };
        // Send push notification via the Send Notifications API
        // https://pushy.me/docs/api/send-notifications
        pushyAPI.sendPushNotification(data, to, options, function (
          err: any,
          id: any
        ) {
          // Log errors to console
          if (err) {
            return console.log("Fatal Error", err);
          }

          // Log success
          console.log("Push sent successfully! (ID: " + id + ")");
        });
      }
    );
  }
}

const onesignalRouter = new OnesignalRouter();
onesignalRouter.routes();

export default onesignalRouter.router;
