import { Request, Response, Router, NextFunction } from "express";
import userSchema from "../models/user";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";
dotenv.config({ path: "variables.env" });
var bcrypt = require("bcryptjs");
var passport = require("passport");
require("./passport")();

var createToken = function (auth: any) {
  return jwt.sign(
    {
      id: auth.id,
    },
    process.env.SECRETO || "secretToken",
    {
      expiresIn: 60 * 120,
    }
  );
};

function generateToken(req: any, res: any, next: any) {
  req.token = createToken(req.auth);
  return next();
}

function sendToken(req: any, res: any) {
  res.setHeader("x-auth-token", req.token);
  return res.status(200).send(JSON.stringify(req.user));
}

class SocialRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post(
      "/api/v1/auth/facebook",
      passport.authenticate("facebook-token", { session: false }),
      function (req: any, res: any, next: any) {
        if (!req.user) {
          return res.send(401);
        }
        req.auth = {
          id: req.user.id,
        };

        next();
      },
      generateToken,
      sendToken
    );

    this.router.post(
      "/api/v1/auth/google",
      passport.authenticate("google-token", { session: false }),
      function (req: any, res: any, next: any) {
        if (!req.user) {
          return res.send(401);
        }
        req.auth = {
          id: req.user.id,
        };

        next();
      },
      generateToken,
      sendToken
    );

    this.router.post("/api/v1/auth/social/mobile", async function (
      req: Request,
      res: Response,
      next: NextFunction
    ) {
      let data = req.body;
      // // check if email exists
      let emailExists = await userSchema.findOne({
        email: data.email,
      });

      if (emailExists) {
        bcrypt.genSalt(10, (err: any, salt: any) => {
          if (err) console.log(err);
          bcrypt.hash(data.token, salt, (err: any, hash: any) => {
            if (err) console.log(err);
            userSchema.findOneAndUpdate(
              { email: data.email },
              { password: hash },
              (err, updated) => {
                if (err) {
                  res.status(500).json({ error: err });
                }
                let nuevoUsuario = updated;
                res.json({ nuevoUsuario, token: data.token });
              }
            );
          });
        });
      } else {
        const nuevoUsuario = new userSchema({
          name: data.firstName,
          lastName: data.lastName,
          email: data.email,
          password: data.token,
          isSocial: true,
        });

        nuevoUsuario.id = nuevoUsuario._id;

        nuevoUsuario.save((error: any) => {
          if (error) {
            return res.status(500).send({ error });
          } else {
            return res.json({ nuevoUsuario, token: data.token });
          }
        });
      }
    });
  }
}
const socialRouter = new SocialRouter();
socialRouter.routes();

export default socialRouter.router;
