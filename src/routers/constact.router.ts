import { Request, Response, Router } from "express";
const nodemailer = require("nodemailer");

class ContactRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.post(
      "/contact-for-restaurant",
      (req: Request, res: Response) => {
        const { email, phone, name } = req.body;
        const transporter = nodemailer.createTransport({
          host: "smtp.gmail.com",
          service: "gmail",
          port: 465,
          secure: true,
          auth: {
            user: process.env.EMAIL_ADDRESS,
            pass: process.env.EMAIL_PASSWORD,
          },
        });
        const mailOptions = {
          from: process.env.EMAIL_ADDRESS,
          to: process.env.EMAIL_ADDRESS,
          subject: "Contacto nuevo restaurante",
          text: "Contacto nuevo Restaurante",
          html: `<div><h2>${name}</h2> <h2>${phone}</h2> <h2>${email}</h2></div>`,
        };
        transporter.sendMail(mailOptions, (err: any) => {
          if (err) {
            console.log("err:", err);
          } else {
            res.send("datos recibidos y enviado");
          }
        });
      }
    );
  }
}

const constctRouter = new ContactRouter();
constctRouter.routes();

export default constctRouter.router;
