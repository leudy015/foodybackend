import { Request, Response, Router } from "express";
import orderSchema from "../models/order";

class IndexRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/", (req: Request, res: Response) =>
      res.send("Bienvenido Authentication con TypeScript")
    );

    this.router.get("/update-orden", (req: Request, res: Response) => {
      const { id } = req.query;
      orderSchema.findOneAndUpdate(
        { _id: id },
        {
          $set: {
            isvalored: true,
          },
        },
        (err, order) => {
          // @ts-ignore
          console.log("done update");
        }
      );
    });
  }
}

const indexRouter = new IndexRouter();
indexRouter.routes();

export default indexRouter.router;
