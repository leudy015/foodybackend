import { Request, Response, Router } from "express";
import bcrypt from "bcryptjs";
import { Document, Types } from "mongoose";
import userSchema from "../models/user";
import { TokenValidations } from "../libs/verifyToken";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";
const mailjet = require("node-mailjet").connect(
  process.env.MJ_APIKEY_PUBLIC,
  process.env.MJ_APIKEY_PRIVATE
);

dotenv.config({ path: "variables.env" });

const { ObjectId } = Types;

export interface IUser extends Document {
  name: string;
  lastName: String;
  email: string;
  password: string;
  termAndConditions: Boolean;
}

const crearToken = (usuarioLogin: any, secreto: any, expiresIn: any) => {
  const { _id } = usuarioLogin;

  return jwt.sign({ _id }, secreto, { expiresIn });
};

class userRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  profileUser(req: Request, res: Response) {
    res.json(req.userId);
  }

  async createUser(req: Request, res: Response) {
    const input = req.body;

    const existentEmail = await userSchema.findOne({ email: req.body.email });

    if (existentEmail) {
      res.json({
        success: false,
        messages: "Ya hay un usuario con este email",
        data: null,
      });

      return null;
    } else {
      const newUser = new userSchema({
        name: input.name,
        lastName: input.lastName,
        email: input.email,
        password: input.password,
        termAndConditions: input.termAndConditions,
      });

      newUser.id = newUser._id;

      return new Promise((resolve, rejects) => {
        newUser.save((err) => {
          if (err) {
            rejects(err);
            res.json({
              success: false,
              messages:
                "Hubo un error al crear tu cuenta intentalo de nuevo por favor",
              data: err,
            });
          } else {
            resolve(newUser);
            res.json({
              success: true,
              messages: "Tu cuenta ha sido creada con éxito",
              data: newUser,
            });

            const request = mailjet.post("contact").request({
              Email: input.email,
              IsExcludedFromCampaigns: "false",
              Name: input.name,
            });
            request
              .then((result: any) => {
                console.log(result.body.Data[0].ID);
                const request = mailjet
                  .post("contact")
                  .id(result.body.Data[0].ID)
                  .action("managecontactslists")
                  .request({
                    ContactsLists: [
                      {
                        ListID: 10241868,
                        Action: "addnoforce",
                      },
                    ],
                  });
                request
                  .then((result: any) => {
                    res.json(result.body);
                  })
                  .catch((err: any) => {
                    console.log(err.statusCode);
                  });
              })
              .catch((err: any) => {
                console.log(err.statusCode);
              });
          }
        });
      });
    }
  }

  getUser(req: Request, res: Response) {
    return new Promise(async (resolve, rejects) => {
      await userSchema.findOne(
        { _id: ObjectId(req.params.id) },
        (err, resp) => {
          if (err) {
            rejects(err);
          } else {
            resolve(res.json(resp));
          }
        }
      );
    });
  }

  getUsers(req: Request, res: Response) {
    return new Promise(async (resolve, rejects) => {
      await userSchema.find((err, resp) => {
        if (err) {
          rejects(err);
        } else {
          resolve(res.json(resp));
        }
      });
    });
  }

  async authenticateteUser(req: Request, res: Response) {
    const { email, password } = req.body;

    const userRes = await userSchema.findOne({ email: email });

    if (!userRes) {
      res.json({
        success: false,
        messages: "Aún no de te has registrado en el sistema",
        data: null,
      });
    }

    const comparePassword = await bcrypt.compare(password, userRes!.password);

    if (!comparePassword) {
      res.json({
        success: false,
        messages: "Contraseña incorrecta",
        data: null,
      });
    } else {
      res
        .header(
          "auth-token",
          crearToken(userRes, process.env.SECRETO, "2400hr")
        )
        .json({
          success: true,
          message: "Bienvenido al sistema!",
          data: {
            token: crearToken(userRes, process.env.SECRETO, "2400hr"),
            id: userRes!._id,
            verifyPhone: userRes!.verifyPhone,
          },
        });
    }
  }

  updateUser(req: Request, res: Response) {
    const data = req.body;

    const input = {
      name: data.name,
      lastName: data.lastName,
      email: data.email,
    };

    return new Promise(async (resolve, reject) => {
      await userSchema.findOneAndUpdate(
        { _id: req.params.id },
        input,
        { new: true },
        (error, user) => {
          if (error) {
            reject(error);
            res.json({
              success: false,
              messages: "Hubo un error con su solicitud",
              data: null,
            });
          } else {
            resolve(user);
            res.json({
              success: true,
              messages: "Cuenta actualizada con éxito",
              data: user,
            });
          }
        }
      );
    });
  }

  deleteUser(req: Request, res: Response) {
    userSchema
      .deleteOne({ _id: ObjectId(req.params.id) })
      .then((resp) => {
        if (resp) {
          res.json({
            success: true,
            messages: "Cuenta eliminado con éxito",
            data: resp,
          });
        }
      })
      .catch((error) => console.log(error));
  }

  routes() {
    this.router.get("/profile", TokenValidations, this.profileUser);
    this.router.get("/:id", this.getUser);
    this.router.get("/", this.getUsers);
    this.router.post("/auth", this.authenticateteUser);
    this.router.post("/", this.createUser);
    this.router.put("/:id", this.updateUser);
    this.router.delete("/:id", this.deleteUser);
  }
}

const usersRouter = new userRouter();

export default usersRouter.router;
