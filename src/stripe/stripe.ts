import { Request, Response, Router } from "express";
const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
import dotenv from "dotenv";
import userSchema from "../models/user";
dotenv.config({ path: "variables.env" });
import { chargeToken } from "./changeToken";
import orderSchema from "../models/order";
import cardSchema from "../models/card";

class StripeRouter {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/create-client", async (req: Request, res: Response) => {
      const { userID, nameclient, email } = req.query;
      await stripe.customers.create(
        {
          name: nameclient,
          email: email,
          description: "Clientes de Foody App",
        },
        function (err: any, customer: any) {
          userSchema.findOneAndUpdate(
            { _id: userID },
            {
              $set: {
                StripeID: customer.id,
              },
            },
            (err, customers) => {
              if (err) {
                console.log(err);
              }
            }
          );
        }
      );
    });

    this.router.get("/card-create", async (req: Request, res: Response) => {
      const { customers, token } = req.query;
      stripe.customers.createSource(customers, { source: token }, function (
        err: any,
        card: any
      ) {
        if (err) {
          res.json(err.raw.code);
        } else {
          res.json(card);
        }
      });
    });

    this.router.get("/get-card", async (req: Request, res: Response) => {
      const { customers } = req.query;
      const paymentMethods = await stripe.paymentMethods.list({
        customer: customers,
        type: "card",
      });
      res.json(paymentMethods);
    });

    this.router.get("/delete-card", async (req: Request, res: Response) => {
      const { cardID, customers } = req.query;
      await stripe.customers.deleteSource(customers, cardID, function (
        err: any,
        confirmation: any
      ) {
        res.json(confirmation);
        console.log(err);
      });
    });

    this.router.get(
      "/payment-existing-card",
      async (req: Request, res: Response) => {
        const { card, customers, amount, order, cart, total } = req.query;
        try {
          await stripe.paymentIntents.create(
            {
              amount: amount,
              currency: "eur",
              customer: customers,
              payment_method: card,
              off_session: true,
              confirm: true,
            },
            function (err: any, payment: any) {
              if (err) {
                console.log(err);
                res.json(err);
              } else {
                res.json(payment);
                console.log(payment);
                if (payment.status === "succeeded") {
                  orderSchema.findOneAndUpdate(
                    { _id: order },
                    {
                      $set: {
                        estado: "Pagada",
                        progreso: "25",
                        status: "active",
                        stripePaymentIntent: payment,
                        total: total,
                      },
                    },
                    (err, order) => {
                      // @ts-ignore
                      cardSchema.deleteMany(
                        // @ts-ignore
                        { restaurant: cart },
                        (err: any, cart: any) => {
                          if (err) console.log(err);
                          else console.log("done deleted");
                        }
                      );
                    }
                  );
                }
              }
            }
          );
        } catch (err) {
          // Error code will be authentication_required if authentication is needed
          console.log("Error code is: ", err);
          const paymentIntentRetrieved = await stripe.paymentIntents.retrieve(
            err.raw.payment_intent.id
          );
          console.log("PI retrieved: ", paymentIntentRetrieved.id);
        }
      }
    );

    this.router.get("/stripe/chargeToken", (req: Request, res: Response) => {
      let data = req.query;
      if (data.stripeToken && data.amount != undefined) {
        chargeToken(data.amount, data.stripeToken)
          .then((response: any) => {
            if (response.status == "succeeded") {
              console.log("pagado actualizar orden");
            } else {
              res.status(401).send({ success: false, data: response });
            }
          })
          .catch((err: any) => {
            res.status(401).send({ success: false, error: err });
          });
      } else {
        res.status(403).send({ success: false, error: "Invalid token" });
      }
    });

    this.router.post("/create-card", async (req: Request, res: Response) => {
      const { customer, paymentMethod } = req.body;
      const paymentMethods = await stripe.paymentMethods.attach(
        paymentMethod.id,
        {
          customer: customer,
        }
      );

      if (paymentMethods) {
        res
          .status(200)
          .send({ success: true, data: "Método de pago añadido con éxito" });
      } else {
        res
          .status(404)
          .send({ success: false, data: "Algo salio mas intentalo de nuevo" });
      }
    });

    this.router.get("/delete-card-web", async (req: Request, res: Response) => {
      const { cardID } = req.query;
      const done = await stripe.paymentMethods.detach(cardID);
      if (done) {
        res.send(done);
      } else {
        console.log("hay un error");
      }
    });
  }
}

const stripeRouter = new StripeRouter();
stripeRouter.routes();

export default stripeRouter.router;
