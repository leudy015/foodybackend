const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);

export const chargeToken = async (amount: any, stripeToken: any) => {
  let response = await stripe.charges.create({
    amount: amount,
    currency: "EUR",
    source: stripeToken,
    capture: true,
  });
  return response;
};
