"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const order_1 = __importDefault(require("../models/order"));
class IndexRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/", (req, res) => res.send("Bienvenido Authentication con TypeScript"));
        this.router.get("/update-orden", (req, res) => {
            const { id } = req.query;
            order_1.default.findOneAndUpdate({ _id: id }, {
                $set: {
                    isvalored: true,
                },
            }, (err, order) => {
                // @ts-ignore
                console.log("done update");
            });
        });
    }
}
const indexRouter = new IndexRouter();
indexRouter.routes();
exports.default = indexRouter.router;
