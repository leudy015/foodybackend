"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const nodemailer = require("nodemailer");
class ContactRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.post("/contact-for-restaurant", (req, res) => {
            const { email, phone, name } = req.body;
            const transporter = nodemailer.createTransport({
                host: "smtp.gmail.com",
                service: "gmail",
                port: 465,
                secure: true,
                auth: {
                    user: process.env.EMAIL_ADDRESS,
                    pass: process.env.EMAIL_PASSWORD,
                },
            });
            const mailOptions = {
                from: process.env.EMAIL_ADDRESS,
                to: process.env.EMAIL_ADDRESS,
                subject: "Contacto nuevo restaurante",
                text: "Contacto nuevo Restaurante",
                html: `<div><h2>${name}</h2> <h2>${phone}</h2> <h2>${email}</h2></div>`,
            };
            transporter.sendMail(mailOptions, (err) => {
                if (err) {
                    console.log("err:", err);
                }
                else {
                    res.send("datos recibidos y enviado");
                }
            });
        });
    }
}
const constctRouter = new ContactRouter();
constctRouter.routes();
exports.default = constctRouter.router;
