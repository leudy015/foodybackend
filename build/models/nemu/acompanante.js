"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const AcompananteSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: true,
        text: true,
    },
    required: {
        type: Boolean,
        default: false,
    },
    children: [{ type: mongoose_1.default.Schema.Types.Mixed }],
    plato: {
        type: String,
        required: true,
    },
    restaurant: {
        type: String,
    },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("acompanante", AcompananteSchema);
