"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const PlatoSchema = new mongoose_1.default.Schema({
    title: {
        type: String,
        required: true,
        text: true,
    },
    ingredientes: {
        type: String,
        text: true,
    },
    price: {
        type: String,
        required: true,
    },
    imagen: {
        type: String,
    },
    restaurant: {
        type: String,
    },
    menu: {
        type: String,
    },
    oferta: {
        type: Boolean,
        default: false,
    },
    popular: {
        type: Boolean,
        default: false,
    },
    news: {
        type: Boolean,
        default: false,
    },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("plato", PlatoSchema);
