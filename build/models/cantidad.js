"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const CantidadSchema = new mongoose_1.default.Schema({
    cant: {
        type: Number,
        required: true,
        default: 1,
    },
    plato: {
        type: String,
        required: true,
    },
});
exports.default = mongoose_1.default.model("cantidad", CantidadSchema);
