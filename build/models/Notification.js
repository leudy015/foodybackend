"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const NotificationSchema = new mongoose_1.default.Schema({
    user: { type: String },
    usuario: { type: String },
    restaurant: { type: String },
    type: { type: String },
    read: { type: mongoose_1.default.Schema.Types.Boolean, default: false },
    ordenId: { type: String },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
exports.default = mongoose_1.default.model("notification", NotificationSchema);
