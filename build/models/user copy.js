"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
mongoose_1.default.Promise = global.Promise;
const userSchema = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: true,
        text: true,
    },
    lastName: {
        type: String,
        required: true,
        text: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
        unique: true,
    },
    termAndConditions: {
        type: Boolean,
        required: true,
        default: false,
    },
    isSocial: {
        type: Boolean,
    },
    verifyPhone: {
        type: Boolean,
        default: false,
    },
    telefono: {
        type: String,
    },
}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });
// hashear los password antes de guardar
userSchema.pre("save", function (next) {
    // Si el password no esta hasheado...
    if (!this.isModified("password")) {
        return next();
    }
    bcryptjs_1.default.genSalt(10, (err, salt) => {
        if (err)
            return next(err);
        bcryptjs_1.default.hash(this.password, salt, (err, hash) => {
            if (err)
                return next(err);
            this.password = hash;
            next();
        });
    });
});
exports.default = mongoose_1.default.model("user", userSchema);
