"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
mongoose_1.default.Promise = global.Promise;
const restaurantSchema = new mongoose_1.default.Schema({
    title: {
        type: String,
        required: true,
        text: true,
    },
    image: {
        type: String,
        required: true,
        text: true,
    },
    description: {
        type: String,
        required: true,
    },
    rating: {
        type: String,
    },
    address: {
        type: mongoose_1.default.Schema.Types.Mixed,
    },
    coordetate: {
        type: String,
    },
    coordenateone: {
        type: String,
    },
    categoryName: {
        type: String,
    },
    categoryID: {
        type: String,
    },
    minime: {
        type: Number,
    },
    menu: {
        type: String,
    },
    phone: {
        type: String,
    },
    email: {
        type: String,
    },
    logo: {
        type: String,
    },
    password: {
        type: String,
    },
    city: {
        type: String,
    },
    type: {
        type: String,
    },
    apertura: {
        type: String,
    },
    cierre: {
        type: String,
    },
    diaslaborales: {
        type: [String],
    },
    isnew: {
        type: Boolean,
        default: true,
    },
    open: {
        type: Boolean,
        default: true,
    },
    OnesignalID: {
        type: String,
    },
});
// hashear los password antes de guardar
restaurantSchema.pre("save", function (next) {
    // Si el password no esta hasheado...
    if (!this.isModified("password")) {
        return next();
    }
    bcryptjs_1.default.genSalt(10, (err, salt) => {
        if (err)
            return next(err);
        bcryptjs_1.default.hash(this.password, salt, (err, hash) => {
            if (err)
                return next(err);
            this.password = hash;
            next();
        });
    });
});
exports.default = mongoose_1.default.model("restaurant", restaurantSchema);
