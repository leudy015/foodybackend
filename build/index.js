"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const helmet_1 = __importDefault(require("helmet"));
const mongoose_1 = __importDefault(require("mongoose"));
const compression_1 = __importDefault(require("compression"));
const cors_1 = __importDefault(require("cors"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const apollo_server_express_1 = require("apollo-server-express");
const path_1 = __importDefault(require("path"));
const body_parser_1 = __importDefault(require("body-parser"));
const index_router_1 = __importDefault(require("./routers/index.router"));
const Apis_1 = __importDefault(require("./LoginSocial/Apis"));
const Recovery_1 = __importDefault(require("./RecoveryPassword/Recovery"));
const Paypal_1 = __importDefault(require("./Paypal"));
const ConfirmNumber_1 = __importDefault(require("./Twilio/ConfirmNumber"));
const userRouter_1 = __importDefault(require("./routers/userRouter"));
const stripe_1 = __importDefault(require("./stripe/stripe"));
const OneSignal_1 = __importDefault(require("./OneSignal"));
const constact_router_1 = __importDefault(require("./routers/constact.router"));
const Resolvers_1 = require("./GraphQL/Resolvers");
const Schema_1 = require("./GraphQL/Schema");
const consolidate_1 = __importDefault(require("consolidate"));
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.router();
    }
    config() {
        const MONGO_URI = process.env.MONGO_URI || "nodata";
        mongoose_1.default.set("useFindAndModify", true);
        mongoose_1.default
            .connect(MONGO_URI, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
        })
            .then((db) => console.log("DB is conected"));
        //Settings
        this.app.set("port", process.env.PORT || 4000);
        //Middleware
        this.app.use(morgan_1.default("dev"));
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: false }));
        this.app.use(helmet_1.default());
        this.app.use(compression_1.default());
        this.app.use(cors_1.default());
        this.app.use(function (req, res, next) {
            //to allow cross domain requests to send cookie information.
            // @ts-ignore-start
            res.setHeader("Access-Control-Allow-Credentials", true);
            // @ts-ignore-end
            // origin can not be '*' when crendentials are enabled. so need to set it to the request origin
            // @ts-nocheck
            res.setHeader("Access-Control-Allow-Origin", "*");
            // list of methods that are supported by the server
            res.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
            res.setHeader("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
            next();
        });
    }
    router() {
        this.app.use(index_router_1.default);
        this.app.use("/api/user", userRouter_1.default);
        this.app.use(Apis_1.default);
        this.app.use(Recovery_1.default);
        this.app.use(ConfirmNumber_1.default);
        this.app.use(Paypal_1.default);
        this.app.use(constact_router_1.default);
        this.app.engine("ejs", consolidate_1.default.ejs);
        this.app.set("views", "views");
        this.app.set("view engine", "ejs");
        this.app.use("/assets", express_1.default.static(path_1.default.join(__dirname + "/../uploads")));
        this.app.use(stripe_1.default);
        this.app.use(OneSignal_1.default);
        this.app.use(body_parser_1.default.urlencoded({
            parameterLimit: 100000,
            limit: "50mb",
            extended: true,
        }));
        this.app.use(body_parser_1.default.json({ limit: "50mb", type: "application/json" }));
    }
    start(paht) {
        this.app.listen(this.app.get("port"), () => {
            console.log(`Server on port http://localhost:${this.app.get("port")}${paht}`);
        });
    }
}
const servers = new apollo_server_express_1.ApolloServer({
    resolvers: Resolvers_1.resolvers,
    typeDefs: Schema_1.typeDefs,
    context: ({ req }) => __awaiter(void 0, void 0, void 0, function* () {
        const token = req.headers["authorization"];
        if (token !== "null") {
            try {
                // verificarl el token que viene del cliente
                const usuarioActual = jsonwebtoken_1.default.verify(token || "tokens", process.env.SECRETO || "mysecretToken");
                req.usuarioActual = usuarioActual;
                return {
                    usuarioActual,
                };
            }
            catch (err) {
                // console.log('err: ', err);
            }
        }
    }),
});
const server = new Server();
servers.applyMiddleware(server);
server.start(servers.graphqlPath);
