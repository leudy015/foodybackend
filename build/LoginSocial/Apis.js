"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_1 = __importDefault(require("../models/user"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: "variables.env" });
var bcrypt = require("bcryptjs");
var passport = require("passport");
require("./passport")();
var createToken = function (auth) {
    return jsonwebtoken_1.default.sign({
        id: auth.id,
    }, process.env.SECRETO || "secretToken", {
        expiresIn: 60 * 120,
    });
};
function generateToken(req, res, next) {
    req.token = createToken(req.auth);
    return next();
}
function sendToken(req, res) {
    res.setHeader("x-auth-token", req.token);
    return res.status(200).send(JSON.stringify(req.user));
}
class SocialRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.post("/api/v1/auth/facebook", passport.authenticate("facebook-token", { session: false }), function (req, res, next) {
            if (!req.user) {
                return res.send(401);
            }
            req.auth = {
                id: req.user.id,
            };
            next();
        }, generateToken, sendToken);
        this.router.post("/api/v1/auth/google", passport.authenticate("google-token", { session: false }), function (req, res, next) {
            if (!req.user) {
                return res.send(401);
            }
            req.auth = {
                id: req.user.id,
            };
            next();
        }, generateToken, sendToken);
        this.router.post("/api/v1/auth/social/mobile", function (req, res, next) {
            return __awaiter(this, void 0, void 0, function* () {
                let data = req.body;
                // // check if email exists
                let emailExists = yield user_1.default.findOne({
                    email: data.email,
                });
                if (emailExists) {
                    bcrypt.genSalt(10, (err, salt) => {
                        if (err)
                            console.log(err);
                        bcrypt.hash(data.token, salt, (err, hash) => {
                            if (err)
                                console.log(err);
                            user_1.default.findOneAndUpdate({ email: data.email }, { password: hash }, (err, updated) => {
                                if (err) {
                                    res.status(500).json({ error: err });
                                }
                                let nuevoUsuario = updated;
                                res.json({ nuevoUsuario, token: data.token });
                            });
                        });
                    });
                }
                else {
                    const nuevoUsuario = new user_1.default({
                        name: data.firstName,
                        lastName: data.lastName,
                        email: data.email,
                        password: data.token,
                        isSocial: true,
                    });
                    nuevoUsuario.id = nuevoUsuario._id;
                    nuevoUsuario.save((error) => {
                        if (error) {
                            return res.status(500).send({ error });
                        }
                        else {
                            return res.json({ nuevoUsuario, token: data.token });
                        }
                    });
                }
            });
        });
    }
}
const socialRouter = new SocialRouter();
socialRouter.routes();
exports.default = socialRouter.router;
