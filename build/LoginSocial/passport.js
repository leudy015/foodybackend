"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const user_1 = __importDefault(require("../models/user"));
var passport = require("passport");
var FacebookTokenStrategy = require("passport-facebook-token");
var GoogleTokenStrategy = require("passport-google-token").Strategy;
module.exports = function () {
    passport.use(new FacebookTokenStrategy({
        clientID: config_1.FACEBOOK_APP_ID,
        clientSecret: config_1.FACEBOOK_SECRET,
    }, (accessToken, refreshToken, profile, done) => __awaiter(this, void 0, void 0, function* () {
        require("mongoose").model("user").schema.add({ isSocial: Boolean });
        let emailExists = yield user_1.default.findOne({
            email: profile.emails[0].value,
        });
        if (emailExists) {
            bcryptjs_1.default.genSalt(10, (err, salt) => {
                if (err)
                    console.log(err);
                bcryptjs_1.default.hash(accessToken, salt, (err, hash) => {
                    if (err)
                        console.log(err);
                    user_1.default.findOneAndUpdate({ email: profile.emails[0].value }, { password: hash }, (err, updated) => {
                        if (err)
                            console.log(err);
                        let nuevoUsuario = updated;
                        return done(err, { nuevoUsuario, token: accessToken });
                    });
                });
            });
        }
        else {
            const nuevoUsuario = new user_1.default({
                id: profile.id,
                password: accessToken,
                email: profile.emails[0].value,
                name: profile.name.givenName,
                lastName: profile.name.familyName,
                isSocial: true,
            });
            nuevoUsuario.id = nuevoUsuario._id;
            nuevoUsuario.save((error) => {
                return done(error, { nuevoUsuario, token: accessToken });
            });
        }
    })));
    passport.use(new GoogleTokenStrategy({
        clientID: config_1.GOOGLE_CLIENT_ID,
        clientSecret: config_1.google_customer_secret,
    }, (accessToken, refreshToken, profile, done) => __awaiter(this, void 0, void 0, function* () {
        require("mongoose").model("user").schema.add({ isSocial: Boolean });
        console.log(profile);
        // check if email exists
        let emailExists = yield user_1.default.findOne({
            email: profile.emails[0].value,
        });
        if (emailExists) {
            bcryptjs_1.default.genSalt(10, (err, salt) => {
                if (err)
                    console.log(err);
                bcryptjs_1.default.hash(accessToken, salt, (err, hash) => {
                    if (err)
                        console.log(err);
                    user_1.default.findOneAndUpdate({ email: profile.emails[0].value }, { password: hash }, (err, updated) => {
                        if (err)
                            console.log(err);
                        console.log(updated);
                        let nuevoUsuario = updated;
                        return done(err, { nuevoUsuario, token: accessToken });
                    });
                });
            });
        }
        else {
            const nuevoUsuario = new user_1.default({
                id: profile.id,
                password: accessToken,
                email: profile.emails[0].value,
                name: profile.name.givenName,
                lastName: profile.name.familyName,
                isSocial: true,
            });
            nuevoUsuario.id = nuevoUsuario._id;
            nuevoUsuario.save((error) => {
                return done(error, { nuevoUsuario, token: accessToken });
            });
        }
    })));
};
