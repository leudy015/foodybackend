"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const paypal_rest_sdk_1 = __importDefault(require("paypal-rest-sdk"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: "variables.env" });
const order_1 = __importDefault(require("../models/order"));
const card_1 = __importDefault(require("../models/card"));
paypal_rest_sdk_1.default.configure({
    mode: "live",
    client_id: process.env.PAYPALCLIENTID || "",
    client_secret: process.env.PAYPALCLIENTSECRET || "",
});
class PaypalRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/paypal", (req, res) => {
            const { price, order, cart } = req.query;
            let create_payment_json = {
                intent: "sale",
                payer: {
                    payment_method: "paypal",
                },
                redirect_urls: {
                    return_url: `https://api.foodyapp.es/success?price=${price}&order=${order}&cart=${cart}`,
                    cancel_url: "https://api.foodyapp.es/cancel",
                },
                transactions: [
                    {
                        amount: {
                            currency: "EUR",
                            total: req.query.price,
                        },
                        description: "Foody Pick Up App S.L",
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.create(create_payment_json, function (error, payment) {
                if (error) {
                    console.log(error);
                    throw error;
                }
                else {
                    console.log("Create Payment Response");
                    console.log(payment);
                    // @ts-ignore
                    res.redirect(payment.links[1].href);
                }
            });
        });
        this.router.get("/success", (req, res) => {
            var PayerID = req.query.PayerID;
            var paymentId = req.query.paymentId;
            const { price, order, cart } = req.query;
            var execute_payment_json = {
                payer_id: PayerID,
                transactions: [
                    {
                        amount: {
                            currency: "EUR",
                            total: price,
                        },
                    },
                ],
            };
            // @ts-ignore
            paypal_rest_sdk_1.default.payment.execute(paymentId, execute_payment_json, function (error, payment) {
                if (error) {
                    console.log(error.response);
                    throw error;
                }
                if (payment) {
                    order_1.default.findOneAndUpdate({ _id: order }, {
                        $set: {
                            estado: "Pagada",
                            progreso: "25",
                            status: "active",
                            pagoPaypal: payment,
                            total: price,
                        },
                    }, (err, order) => {
                        // @ts-ignore
                        card_1.default.deleteMany(
                        // @ts-ignore
                        { restaurant: cart }, (err, cart) => {
                            if (err)
                                console.log(err);
                            else
                                console.log("done update");
                        });
                        res.render("success");
                    });
                    console.log("Get Payment Response");
                }
            });
        });
        this.router.get("/cancel", (req, res) => {
            res.render("cancel");
        });
    }
}
const paypalRouter = new PaypalRouter();
paypalRouter.routes();
exports.default = paypalRouter.router;
