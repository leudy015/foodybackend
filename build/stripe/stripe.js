"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const stripe = require("stripe")(process.env.STRIPECLIENTSECRET);
const dotenv_1 = __importDefault(require("dotenv"));
const user_1 = __importDefault(require("../models/user"));
dotenv_1.default.config({ path: "variables.env" });
const changeToken_1 = require("./changeToken");
const order_1 = __importDefault(require("../models/order"));
const card_1 = __importDefault(require("../models/card"));
class StripeRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/create-client", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { userID, nameclient, email } = req.query;
            yield stripe.customers.create({
                name: nameclient,
                email: email,
                description: "Clientes de Foody App",
            }, function (err, customer) {
                user_1.default.findOneAndUpdate({ _id: userID }, {
                    $set: {
                        StripeID: customer.id,
                    },
                }, (err, customers) => {
                    if (err) {
                        console.log(err);
                    }
                });
            });
        }));
        this.router.get("/card-create", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { customers, token } = req.query;
            stripe.customers.createSource(customers, { source: token }, function (err, card) {
                if (err) {
                    res.json(err.raw.code);
                }
                else {
                    res.json(card);
                }
            });
        }));
        this.router.get("/get-card", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { customers } = req.query;
            const paymentMethods = yield stripe.paymentMethods.list({
                customer: customers,
                type: "card",
            });
            res.json(paymentMethods);
        }));
        this.router.get("/delete-card", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { cardID, customers } = req.query;
            yield stripe.customers.deleteSource(customers, cardID, function (err, confirmation) {
                res.json(confirmation);
                console.log(err);
            });
        }));
        this.router.get("/payment-existing-card", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { card, customers, amount, order, cart, total } = req.query;
            try {
                yield stripe.paymentIntents.create({
                    amount: amount,
                    currency: "eur",
                    customer: customers,
                    payment_method: card,
                    off_session: true,
                    confirm: true,
                }, function (err, payment) {
                    if (err) {
                        console.log(err);
                        res.json(err);
                    }
                    else {
                        res.json(payment);
                        console.log(payment);
                        if (payment.status === "succeeded") {
                            order_1.default.findOneAndUpdate({ _id: order }, {
                                $set: {
                                    estado: "Pagada",
                                    progreso: "25",
                                    status: "active",
                                    stripePaymentIntent: payment,
                                    total: total,
                                },
                            }, (err, order) => {
                                // @ts-ignore
                                card_1.default.deleteMany(
                                // @ts-ignore
                                { restaurant: cart }, (err, cart) => {
                                    if (err)
                                        console.log(err);
                                    else
                                        console.log("done deleted");
                                });
                            });
                        }
                    }
                });
            }
            catch (err) {
                // Error code will be authentication_required if authentication is needed
                console.log("Error code is: ", err);
                const paymentIntentRetrieved = yield stripe.paymentIntents.retrieve(err.raw.payment_intent.id);
                console.log("PI retrieved: ", paymentIntentRetrieved.id);
            }
        }));
        this.router.get("/stripe/chargeToken", (req, res) => {
            let data = req.query;
            if (data.stripeToken && data.amount != undefined) {
                changeToken_1.chargeToken(data.amount, data.stripeToken)
                    .then((response) => {
                    if (response.status == "succeeded") {
                        console.log("pagado actualizar orden");
                    }
                    else {
                        res.status(401).send({ success: false, data: response });
                    }
                })
                    .catch((err) => {
                    res.status(401).send({ success: false, error: err });
                });
            }
            else {
                res.status(403).send({ success: false, error: "Invalid token" });
            }
        });
        this.router.post("/create-card", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { customer, paymentMethod } = req.body;
            const paymentMethods = yield stripe.paymentMethods.attach(paymentMethod.id, {
                customer: customer,
            });
            if (paymentMethods) {
                res
                    .status(200)
                    .send({ success: true, data: "Método de pago añadido con éxito" });
            }
            else {
                res
                    .status(404)
                    .send({ success: false, data: "Algo salio mas intentalo de nuevo" });
            }
        }));
        this.router.get("/delete-card-web", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { cardID } = req.query;
            const done = yield stripe.paymentMethods.detach(cardID);
            if (done) {
                res.send(done);
            }
            else {
                console.log("hay un error");
            }
        }));
    }
}
const stripeRouter = new StripeRouter();
stripeRouter.routes();
exports.default = stripeRouter.router;
