"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_1 = __importDefault(require("../models/user"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
var Pushy = require("pushy");
class OnesignalRouter {
    constructor() {
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get("/save-userid-notification", (req, res) => {
            const { OnesignalID, id } = req.query;
            if (OnesignalID && id) {
                user_1.default
                    .findOneAndUpdate({ _id: id }, { $set: { OnesignalID: OnesignalID } }, (err, user) => { })
                    .catch((err) => {
                    console.log(err);
                });
            }
        });
        this.router.get("/save-restaurant-notification", (req, res) => {
            const { OnesignalID, id } = req.query;
            if (OnesignalID && id) {
                restaurant_1.default
                    .findOneAndUpdate({ _id: id }, { $set: { OnesignalID: OnesignalID } }, (err, user) => { })
                    .catch((err) => {
                    console.log(err);
                });
            }
        });
        this.router.get("/send-push-notification", (req, res) => {
            const { IdOnesignal, textmessage } = req.query;
            var sendNotification = function (data) {
                var headers = {
                    "Content-Type": "application/json; charset=utf-8",
                };
                var options = {
                    host: "onesignal.com",
                    port: 443,
                    path: "/api/v1/notifications",
                    method: "POST",
                    headers: headers,
                };
                var https = require("https");
                var req = https.request(options, function (res) {
                    res.on("data", function (data) {
                        console.log("Response:");
                        console.log(JSON.parse(data));
                    });
                });
                req.on("error", function (e) {
                    console.log("ERROR:");
                    console.log(e);
                });
                req.write(JSON.stringify(data));
                req.end();
            };
            var message = {
                app_id: "db79b975-e551-4741-ae43-8968ceab5f09",
                headings: { en: "Foody App" },
                contents: { en: `${textmessage}` },
                ios_badgeCount: 1,
                ios_badgeType: "Increase",
                include_player_ids: [`${IdOnesignal}`],
            };
            sendNotification(message);
        });
        this.router.get("/send-push-notification-restaurant", (req, res) => {
            const { IdOnesignal, textmessage } = req.query;
            // Plug in your Secret API Key
            // Get it here: https://dashboard.pushy.me/
            var pushyAPI = new Pushy("76b0ec4e467bd35c5d857c85949f48cf8951d98a6ccd9b94a0c7d894dfcecb6b");
            // Set push payload data to deliver to device(s)
            var data = {
                message: textmessage,
            };
            // Insert target device token(s) here
            var to = [IdOnesignal];
            // Optionally, send to a publish/subscribe topic instead
            // to = '/topics/news';
            // Set optional push notification options (such as iOS notification fields)
            var options = {
                notification: {
                    badge: 1,
                    sound: "ping.aiff",
                    body: "Nuevo pedido \u270c",
                },
            };
            // Send push notification via the Send Notifications API
            // https://pushy.me/docs/api/send-notifications
            pushyAPI.sendPushNotification(data, to, options, function (err, id) {
                // Log errors to console
                if (err) {
                    return console.log("Fatal Error", err);
                }
                // Log success
                console.log("Push sent successfully! (ID: " + id + ")");
            });
        });
    }
}
const onesignalRouter = new OnesignalRouter();
onesignalRouter.routes();
exports.default = onesignalRouter.router;
