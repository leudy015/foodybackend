"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.resolvers = void 0;
const query_1 = require("./query");
const mutation_1 = require("./mutation");
const user_1 = __importDefault(require("../models/user"));
const Favorito_1 = __importDefault(require("../models/Favorito"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const platos_1 = __importDefault(require("../models/nemu/platos"));
const rating_1 = __importDefault(require("../models/rating"));
const order_1 = __importDefault(require("../models/order"));
const Opinion_1 = __importDefault(require("../models/Opinion"));
exports.resolvers = {
    Query: query_1.Query,
    Mutation: mutation_1.Mutation,
    Platos: {
        opiniones(parent, args) {
            return new Promise((resolve, rejects) => {
                Opinion_1.default.find({ plato: parent._id }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
    },
    Opinion: {
        Usuario(parent, args) {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: parent.user }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
    },
    Notification: {
        Usuarios(parent, args) {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: parent.usuario }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
        Restaurant(parent, args) {
            return new Promise((resolve, rejects) => {
                restaurant_1.default.findOne({ _id: parent.restaurant }, (error, restaurant) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!restaurant)
                            resolve(false);
                        else {
                            resolve(restaurant);
                        }
                    }
                });
            });
        },
        Orden(parent, args) {
            return new Promise((resolve, rejects) => {
                order_1.default.findOne({ _id: parent.ordenId }, (error, orden) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!orden)
                            resolve(false);
                        else {
                            resolve(orden);
                        }
                    }
                });
            });
        },
    },
    Valoracion: {
        Usuario(parent, args) {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: parent.user }, (error, user) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!user)
                            resolve(false);
                        else {
                            resolve(user);
                        }
                    }
                });
            });
        },
    },
    Orden: {
        restaurants(parent, args) {
            return new Promise((resolve, rejects) => {
                restaurant_1.default.findOne({ _id: parent.restaurant }, (error, restaurant) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!restaurant)
                            resolve(false);
                        else {
                            resolve(restaurant);
                        }
                    }
                });
            });
        },
        usuario(parent, args) {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: parent.userID }, (error, userID) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!userID)
                            resolve(false);
                        else {
                            resolve(userID);
                        }
                    }
                });
            });
        },
    },
    Menu: {
        platos(parent, args) {
            return new Promise((resolve, rejects) => {
                platos_1.default.find({ menu: parent._id }, (error, plato) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!plato)
                            resolve(false);
                        else {
                            resolve(plato);
                        }
                    }
                }).sort({ $natural: -1 });
            });
        },
    },
    Restaurant: {
        anadidoFavorito(parent, args, { usuarioActual }) {
            return new Promise((resolve, reject) => {
                if (!usuarioActual)
                    resolve(false);
                user_1.default.findOne({ _id: usuarioActual._id }, (error, usuario) => {
                    if (error)
                        reject(error);
                    else {
                        if (!usuario)
                            resolve(false);
                        else {
                            Favorito_1.default.findOne({ usuarioId: usuario._id, restaurantID: parent._id }, (error, favorito) => {
                                if (error)
                                    reject(error);
                                else {
                                    if (!favorito)
                                        resolve(false);
                                    else
                                        resolve(true);
                                }
                            });
                        }
                    }
                });
            });
        },
        Valoracion(parent, args) {
            return new Promise((resolve, rejects) => {
                rating_1.default.find({ restaurant: parent._id }, (error, valoracion) => {
                    if (error)
                        rejects(error);
                    else {
                        if (!valoracion)
                            resolve(false);
                        else {
                            resolve(valoracion);
                        }
                    }
                });
            });
        },
    },
    RestaurantFavorito: {
        restaurant(parent) {
            return new Promise((resolve, reject) => {
                restaurant_1.default.findById({ _id: parent.restaurantID }, (error, restaurant) => {
                    if (error) {
                        console.log("erro in chiild: ", error);
                        reject(error);
                    }
                    else {
                        resolve(restaurant);
                    }
                });
            });
        },
    },
    Cart: {
        Restaurant(parent) {
            return new Promise((resolve, reject) => {
                restaurant_1.default.findById({ _id: parent.restaurant }, (error, restaurant) => {
                    if (error) {
                        console.log("erro in chiild: ", error);
                        reject(error);
                    }
                    else {
                        resolve(restaurant);
                    }
                });
            });
        },
        UserId(parent) {
            return new Promise((resolve, reject) => {
                user_1.default.findById({ _id: parent.userId }, (error, user) => {
                    if (error) {
                        console.log("erro in chiild: ", error);
                        reject(error);
                    }
                    else {
                        resolve(user);
                    }
                });
            });
        },
    },
};
