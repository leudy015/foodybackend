"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Query = void 0;
const user_1 = __importDefault(require("../models/user"));
const categorias_1 = __importDefault(require("../models/categorias"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const menu_1 = __importDefault(require("../models/nemu/menu"));
const acompanante_1 = __importDefault(require("../models/nemu/acompanante"));
const Favorito_1 = __importDefault(require("../models/Favorito"));
const cupones_1 = __importDefault(require("../models/cupones"));
const card_1 = __importDefault(require("../models/card"));
const order_1 = __importDefault(require("../models/order"));
const Status_messages_1 = require("./Status_messages");
const rating_1 = __importDefault(require("../models/rating"));
const Notification_1 = __importDefault(require("../models/Notification"));
const Opinion_1 = __importDefault(require("../models/Opinion"));
const Pago_1 = __importDefault(require("../models/Pago"));
const transacciones_1 = __importDefault(require("../models/transacciones"));
exports.Query = {
    getUsuario: (root, {}, { usuarioActual }) => {
        if (!usuarioActual) {
            return {
                success: false,
                messages: Status_messages_1.STATUS_MESSAGES.NOT_LOGGED_IN,
                data: {},
            };
        }
        else {
            return new Promise((resolve, rejects) => {
                user_1.default.findOne({ _id: usuarioActual._id }, (err, res) => {
                    if (err) {
                        rejects({
                            messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                            success: false,
                            data: {},
                        });
                    }
                    else {
                        resolve({
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            success: true,
                            data: res,
                        });
                    }
                });
            });
        }
    },
    obtenerUsuario: (root, args, { usuarioActual }) => {
        if (!usuarioActual) {
            return null;
        }
        const usuario = user_1.default.findOne({ _id: usuarioActual._id });
        return usuario;
    },
    getCategory: (root) => {
        return new Promise((resolve, rejects) => {
            categorias_1.default.find((err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurant: (root, { city }) => {
        return new Promise((resolve, rejects) => {
            restaurant_1.default.find({ city: city }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantSearch: (root, { city, price, category, search }) => {
        let condition = {};
        if (search)
            condition = { $text: { $search: `"\"${search} \""` } };
        // @ts-ignore
        if (price)
            condition.minime = { $lt: price };
        // @ts-ignore
        if (city)
            condition.city = city;
        // @ts-ignore
        if (category)
            condition.categoryID = category;
        return new Promise((resolve, rejects) => {
            restaurant_1.default.find(condition, { score: { $meta: "textScore" } }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantForCategory: (root, { city, category }) => {
        return new Promise((resolve, rejects) => {
            restaurant_1.default.find({ city: city, categoryID: category }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantForID: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            restaurant_1.default.findOne({ _id: id }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: {},
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            });
        });
    },
    getRestaurantFavorito: (root, { id }) => {
        return new Promise((resolve, reject) => {
            Favorito_1.default.find({ usuarioId: id }, (error, favourite) => {
                if (error) {
                    console.log("error ==>: ", error);
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: favourite,
                    });
                }
            });
        });
    },
    getMenu: (root, { id }) => {
        return new Promise((resolve, reject) => {
            menu_1.default.find({ restaurant: id }, (error, favourite) => {
                if (error) {
                    console.log("error ==>: ", error);
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: favourite,
                    });
                }
            }).sort({ $natural: -1 });
        });
    },
    getAcompanante: (root, { id }) => {
        return new Promise((resolve, reject) => {
            acompanante_1.default.find({ plato: id }, (error, favourite) => {
                if (error) {
                    console.log("error ==>: ", error);
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: favourite,
                    });
                }
            });
        });
    },
    getItemCart: (root, { id, PlatoID }) => {
        return new Promise((resolve, reject) => {
            card_1.default.find({ userId: id, platoID: PlatoID }, (error, cart) => {
                if (error) {
                    console.log("error ==>: ", error);
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: cart,
                    });
                }
            });
        });
    },
    getItemCarts: (root, { id }) => {
        return new Promise((resolve, reject) => {
            card_1.default.find({ userId: id }, (error, cart) => {
                if (error) {
                    console.log("error ==>: ", error);
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: cart,
                    });
                }
            });
        });
    },
    getMyItemCart: (root, { id, restaurant }) => {
        return new Promise((resolve, reject) => {
            card_1.default.find({ userId: id, restaurant: restaurant }, (error, cart) => {
                if (error) {
                    console.log("error ==>: ", error);
                    reject({
                        success: false,
                        message: "Hay un problema con su solicitud",
                        list: [],
                    });
                }
                else {
                    resolve({
                        success: true,
                        message: "Operación realizada con éxito",
                        list: cart,
                    });
                }
            });
        });
    },
    getCupon: (root, { clave }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (!usuarioActual) {
                return {
                    success: false,
                    message: "Debe iniciar sesión para continuar",
                    data: null,
                };
            }
            const usuario = yield user_1.default.findOne({
                _id: usuarioActual._id,
            });
            if (!usuario) {
                return {
                    success: false,
                    message: "Debe iniciar sesión para continuar",
                    data: null,
                };
            }
            return new Promise((resolve, reject) => {
                cupones_1.default.findOne({ clave }, (error, cupon) => {
                    if (error) {
                        console.log("resolvers -> query -> getCupon -> error1 ", error);
                        return reject(error);
                    }
                    else {
                        return resolve(cupon);
                    }
                });
            });
        }
        catch (error) {
            console.log("resolvers -> query -> getCupon -> error2 ", error);
            return {
                success: false,
                message: "Hay un problema con su solicitud",
                data: null,
            };
        }
    }),
    getCuponAll: (root) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            return new Promise((resolve, reject) => {
                cupones_1.default.findOne((error, cupon) => {
                    if (error) {
                        console.log("resolvers -> query -> getCupon -> error1 ", error);
                        return reject(error);
                    }
                    else {
                        return resolve(cupon);
                    }
                });
            });
        }
        catch (error) {
            console.log("resolvers -> query -> getCupon -> error2 ", error);
            return {
                success: false,
                message: "Hay un problema con su solicitud",
                data: null,
            };
        }
    }),
    getOrderByUsuario: (root, { usuarios, dateRange }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                success: false,
                message: "Debe iniciar sesión para continuar",
                list: [],
            };
        }
        const usuario = yield user_1.default.findOne({
            _id: usuarioActual._id,
        });
        if (!usuario) {
            return {
                success: false,
                message: "Debe iniciar sesión para continuar",
                list: [],
            };
        }
        return new Promise((resolve, reject) => {
            let condition = {
                userID: usuarios,
                estado: { $ne: "Pendiente de pago" },
            };
            //let condition = { cliente: "5d8556514c10b81c6a65295a" };
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, consulta) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: consulta,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getOrderByRestaurant: (root, { id, dateRange }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                restaurant: id,
                estado: { $ne: "Pendiente de pago" },
            };
            //let condition = { cliente: "5d8556514c10b81c6a65295a" };
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: order,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getOrderByRestaurantProcess: (root, { id, dateRange }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                restaurant: id,
                estado: {
                    $in: ["Preparando", "Confirmada"],
                },
            };
            //let condition = { cliente: "5d8556514c10b81c6a65295a" };
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: order,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getOrderByRestaurantListos: (root, { id, dateRange }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                restaurant: id,
                estado: {
                    $in: ["Lista para recojer"],
                },
            };
            //let condition = { cliente: "5d8556514c10b81c6a65295a" };
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: order,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getOrderByRestaurantNew: (root, { id, dateRange }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let condition = {
                restaurant: id,
                estado: {
                    $in: ["Pagada"],
                },
            };
            //let condition = { cliente: "5d8556514c10b81c6a65295a" };
            if (dateRange && dateRange.fromDate && dateRange.toDate) {
                // @ts-ignore
                condition.created_at = {
                    $gte: dateRange.fromDate,
                    $lte: dateRange.toDate,
                };
            }
            order_1.default
                .find(condition, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: order,
                    });
                }
            })
                .sort({ $natural: -1 });
        });
    }),
    getOrderByRestaurantID: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            order_1.default.findOne({ _id: id }, (error, order) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        list: [],
                    });
                else {
                    resolve({
                        success: true,
                        message: "Consulta extraida con éxito",
                        list: order,
                    });
                }
            });
        });
    }),
    getValoraciones: (root, { restaurant }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            return new Promise((resolve, reject) => {
                rating_1.default.find({ restaurant: restaurant }, (error, valoracion) => {
                    if (error) {
                        console.log("resolvers -> query -> getCupon -> error1 ", error);
                        return reject(error);
                    }
                    else {
                        resolve({
                            messages: "Datos Obtenidos con éxito",
                            success: true,
                            data: valoracion,
                        });
                    }
                }).sort({ $natural: -1 });
            });
        }
        catch (error) {
            console.log("resolvers -> query -> getCupon -> error2 ", error);
            return {
                success: false,
                message: "Hay un problema con su solicitud",
                data: null,
            };
        }
    }),
    getNotifications: (root, { Id }) => {
        if (Id) {
            return new Promise((resolve, reject) => {
                Notification_1.default.find({ user: Id, read: false })
                    .populate("users")
                    .populate("restaurants")
                    .populate("orders")
                    .sort({ $natural: -1 })
                    .exec((error, notification) => {
                    if (error) {
                        console.log(error);
                        reject({
                            messages: "Hubo un problema con su solicitud",
                            success: false,
                            notifications: [],
                        });
                    }
                    else {
                        resolve({
                            messages: "success",
                            success: true,
                            notifications: notification,
                        });
                    }
                });
            });
        }
        else {
            return null;
        }
    },
    getOpinion: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            Opinion_1.default.find({ plato: id }, (err, res) => {
                if (err) {
                    rejects({
                        messages: Status_messages_1.STATUS_MESSAGES.S_WENT_WRONG,
                        success: false,
                        data: [],
                    });
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                        data: res,
                    });
                }
            }).sort({ $natural: -1 });
        });
    },
    getPago: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            Pago_1.default.findOne({ restaurantID: id }, (error, pagos) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                        data: null,
                    });
                else
                    resolve({
                        messages: "Operacion realizada con éxito",
                        success: true,
                        data: pagos,
                    });
            });
        });
    }),
    getTransaction: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            transacciones_1.default
                .find({ restaurantID: id })
                .sort({ $natural: -1 })
                .exec((error, deposito) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                        list: [],
                    });
                else
                    resolve({
                        messages: "solicitud procesada con éxito",
                        success: true,
                        list: deposito,
                    });
            });
        });
    }),
    getStatistics: (root, { id }) => {
        return new Promise((resolve, reject) => {
            let matchQuery1 = {
                restaurant: id,
                estado: "Recogido",
            };
            let matchQuery2 = {
                restaurant: id,
                estado: "Rechazada",
            };
            order_1.default
                .aggregate([
                { $match: matchQuery1 },
                {
                    $group: {
                        _id: {
                            month: { $substr: ["$created_at", 5, 2] },
                            estado: "Recogido",
                        },
                        stripePaymentIntent: { $first: "$stripePaymentIntent" },
                        pagoPaypal: { $first: "$pagoPaypal" },
                        paypalAmount: { $sum: "$pagoPaypal.montoPagado" },
                        stripeAmount: { $sum: "$stripePaymentIntent.amount" },
                        finishedCount: { $sum: 1 },
                        created_at: { $first: "$created_at" },
                    },
                },
                {
                    $project: {
                        your_year_variable: { $year: "$created_at" },
                        paypalAmount: 1,
                        finishedCount: 1,
                        stripeAmountDivide: { $divide: ["$stripeAmount", 100] },
                    },
                },
                { $match: { your_year_variable: 2020 } },
            ])
                .then((res) => {
                let ordenes = {
                    name: "Pedidos",
                    Ene: 0,
                    Feb: 0,
                    Mar: 0,
                    Abr: 0,
                    May: 0,
                    Jun: 0,
                    Jul: 0,
                    Aug: 0,
                    Sep: 0,
                    Oct: 0,
                    Nov: 0,
                    Dic: 0,
                };
                let ganacias = {
                    name: "Total Ventas",
                    Ene: 0,
                    Feb: 0,
                    Mar: 0,
                    Abr: 0,
                    May: 0,
                    Jun: 0,
                    Jul: 0,
                    Aug: 0,
                    Sep: 0,
                    Oct: 0,
                    Nov: 0,
                    Dic: 0,
                };
                let devoluciones = {
                    name: "Pedidos rechazados",
                    Ene: 0,
                    Feb: 0,
                    Mar: 0,
                    Abr: 0,
                    May: 0,
                    Jun: 0,
                    Jul: 0,
                    Aug: 0,
                    Sep: 0,
                    Oct: 0,
                    Nov: 0,
                    Dic: 0,
                };
                for (let i = 0; i < res.length; i++) {
                    let month = res[i]._id.month;
                    if (month == "01") {
                        ganacias["Ene"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Ene"] = res[i].finishedCount;
                    }
                    else if (month == "02") {
                        ganacias["Feb"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Feb"] = res[i].finishedCount;
                    }
                    else if (month == "03") {
                        ganacias["Mar"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Mar"] = res[i].finishedCount;
                    }
                    else if (month == "04") {
                        ganacias["Abr"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Abr"] = res[i].finishedCount;
                    }
                    else if (month == "05") {
                        ganacias["May"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["May"] = res[i].finishedCount;
                    }
                    else if (month == "06") {
                        ganacias["Jun"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Jun"] = res[i].finishedCount;
                    }
                    else if (month == "07") {
                        ganacias["Jul"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Jul"] = res[i].finishedCount;
                    }
                    else if (month == "08") {
                        ganacias["Aug"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Aug"] = res[i].finishedCount;
                    }
                    else if (month == "09") {
                        ganacias["Sep"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Sep"] = res[i].finishedCount;
                    }
                    else if (month == "10") {
                        ganacias["Oct"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Oct"] = res[i].finishedCount;
                    }
                    else if (month == "11") {
                        ganacias["Nov"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Nov"] = res[i].finishedCount;
                    }
                    else if (month == "12") {
                        ganacias["Dic"] = Math.round((isNaN(res[i].paypalAmount) ? 0 : res[i].paypalAmount) +
                            (isNaN(res[i].stripeAmountDivide)
                                ? 0
                                : res[i].stripeAmountDivide));
                        ordenes["Dic"] = res[i].finishedCount;
                    }
                }
                order_1.default
                    .aggregate([
                    { $match: matchQuery2 },
                    {
                        $group: {
                            _id: {
                                month: { $substr: ["$created_at", 5, 2] },
                                estado: "Rechazada",
                            },
                            returnedCount: { $sum: 1 },
                            created_at: { $first: "$created_at" },
                        },
                    },
                    {
                        $project: {
                            your_year_variable: { $year: "$created_at" },
                            returnedCount: 1,
                        },
                    },
                    { $match: { your_year_variable: 2020 } },
                ])
                    .then((res1) => {
                    for (let i = 0; i < res1.length; i++) {
                        let month = res1[i]._id.month;
                        if (month == "01") {
                            devoluciones["Ene"] = res1[i].returnedCount;
                        }
                        else if (month == "02") {
                            devoluciones["Feb"] = res1[i].returnedCount;
                        }
                        else if (month == "03") {
                            devoluciones["Mar"] = res1[i].returnedCount;
                        }
                        else if (month == "04") {
                            devoluciones["Abr"] = res1[i].returnedCount;
                        }
                        else if (month == "05") {
                            devoluciones["May"] = res1[i].returnedCount;
                        }
                        else if (month == "06") {
                            devoluciones["Jun"] = res1[i].returnedCount;
                        }
                        else if (month == "07") {
                            devoluciones["Jul"] = res1[i].returnedCount;
                        }
                        else if (month == "08") {
                            devoluciones["Aug"] = res1[i].returnedCount;
                        }
                        else if (month == "09") {
                            devoluciones["Sep"] = res1[i].returnedCount;
                        }
                        else if (month == "10") {
                            devoluciones["Oct"] = res1[i].returnedCount;
                        }
                        else if (month == "11") {
                            devoluciones["Nov"] = res1[i].returnedCount;
                        }
                        else if (month == "12") {
                            devoluciones["Dic"] = res1[i].returnedCount;
                        }
                    }
                    resolve({
                        success: true,
                        message: "",
                        data: [ordenes, ganacias, devoluciones],
                    });
                });
            })
                .catch((err) => {
                console.log(err);
            });
        });
    },
};
