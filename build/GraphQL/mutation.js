"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Mutation = void 0;
const categorias_1 = __importDefault(require("../models/categorias"));
const user_1 = __importDefault(require("../models/user"));
const restaurant_1 = __importDefault(require("../models/restaurant"));
const platos_1 = __importDefault(require("../models/nemu/platos"));
const menu_1 = __importDefault(require("../models/nemu/menu"));
const Pago_1 = __importDefault(require("../models/Pago"));
const transacciones_1 = __importDefault(require("../models/transacciones"));
const acompanante_1 = __importDefault(require("../models/nemu/acompanante"));
const card_1 = __importDefault(require("../models/card"));
const cupones_1 = __importDefault(require("../models/cupones"));
const Favorito_1 = __importDefault(require("../models/Favorito"));
const Status_messages_1 = require("./Status_messages");
const fs_1 = __importDefault(require("fs"));
const mongoose_1 = require("mongoose");
const order_1 = __importDefault(require("../models/order"));
const rating_1 = __importDefault(require("../models/rating"));
const Notification_1 = __importDefault(require("../models/Notification"));
const Opinion_1 = __importDefault(require("../models/Opinion"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const mailjet = require("node-mailjet").connect(process.env.MJ_APIKEY_PUBLIC, process.env.MJ_APIKEY_PRIVATE);
const { ObjectId } = mongoose_1.Types;
const crearToken = (restaurant, secreto, expiresIn) => {
    const { _id } = restaurant;
    return jsonwebtoken_1.default.sign({ _id }, secreto, { expiresIn });
};
exports.Mutation = {
    autenticarRestaurant: (root, { email, password }) => __awaiter(void 0, void 0, void 0, function* () {
        const restaurant = yield restaurant_1.default.findOne({ email });
        if (!restaurant) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.USER_NOT_FOUND,
                data: null,
            };
        }
        const passwordCorrecto = yield bcryptjs_1.default.compare(password, restaurant.password);
        if (!passwordCorrecto) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.INCORRECT_PASSWORD,
                data: null,
            };
        }
        return {
            success: true,
            message: "Bienvenido a Foody Restaurante",
            data: {
                token: crearToken(restaurant, process.env.SECRETO, "5000hr"),
                id: restaurant._id,
            },
        };
    }),
    autenticarUsuario: (root, { email, password }) => __awaiter(void 0, void 0, void 0, function* () {
        const users = yield user_1.default.findOne({ email });
        if (!users) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.USER_NOT_FOUND,
                data: null,
            };
        }
        const passwordCorrecto = yield bcryptjs_1.default.compare(password, users.password);
        if (!passwordCorrecto) {
            return {
                success: false,
                message: Status_messages_1.STATUS_MESSAGES.INCORRECT_PASSWORD,
                data: null,
            };
        }
        return {
            success: true,
            message: "Bienvenid@ a Foody",
            data: {
                token: crearToken(users, process.env.SECRETO, "5000hr"),
                id: users._id,
            },
        };
    }),
    crearUsuario: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        // check if email exists
        const emailExists = yield user_1.default.findOne({ email: input.email });
        if (emailExists) {
            return {
                success: false,
                message: "Ya eres parte de Foody",
                data: null,
            };
        }
        const nuevoUsuario = new user_1.default({
            name: input.nombre,
            lastName: input.apellidos,
            email: input.email,
            password: input.password,
            termAndConditions: input.termAndConditions,
        });
        nuevoUsuario.id = nuevoUsuario._id;
        return new Promise((resolve, object) => {
            nuevoUsuario.save((error) => {
                if (error)
                    object({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                        data: null,
                    });
                else {
                    resolve({
                        success: true,
                        message: "Usuario agregado con éxito",
                        data: nuevoUsuario,
                    });
                    const request = mailjet.post("contact").request({
                        Email: input.email,
                        IsExcludedFromCampaigns: "false",
                        Name: input.name,
                    });
                    request
                        .then((result) => {
                        console.log(result.body.Data[0].ID);
                        const request = mailjet
                            .post("contact")
                            .id(result.body.Data[0].ID)
                            .action("managecontactslists")
                            .request({
                            ContactsLists: [
                                {
                                    ListID: 10241868,
                                    Action: "addnoforce",
                                },
                            ],
                        });
                        request
                            .then((result) => {
                            console.log("done");
                        })
                            .catch((err) => {
                            console.log(err.statusCode);
                        });
                    })
                        .catch((err) => {
                        console.log(err.statusCode);
                    });
                }
            });
        });
    }),
    singleUpload(parent, { file }) {
        const matches = file.match(/^data:.+\/(.+);base64,(.*)$/);
        const ext = matches[1];
        const base64_data = matches[2];
        const buffer = Buffer.from(base64_data, "base64");
        const filename = `${Date.now()}-file.${ext}`;
        const filenameWithPath = `${__dirname}/../../uploads/images/${filename}`;
        return new Promise((resolve, reject) => {
            fs_1.default.writeFile(filenameWithPath, buffer, (error) => {
                if (error) {
                    reject(error);
                    console.log(error);
                }
                else {
                    resolve({ filename });
                    console.log(filename);
                }
            });
        });
    },
    createCategory: (root, { input }) => {
        const nuevaCategoria = new categorias_1.default({
            title: input.title,
            image: input.image,
            description: input.description,
        });
        return new Promise((resolve, rejects) => {
            nuevaCategoria.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.USER_ADDED,
                        success: true,
                    });
                }
            });
        });
    },
    eliminarCategory: (root, { id }) => {
        return new Promise((resolve, rejects) => {
            categorias_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    rejects(error);
                else
                    resolve("Eliminado correctamente");
            });
        });
    },
    actualizarUsuario: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            user_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error, _usuario) => {
                if (error)
                    reject(error);
                else
                    resolve(_usuario);
            });
        });
    }),
    eliminarUsuario: (root, { id }) => {
        console.log(id);
        return new Promise((resolve, reject) => {
            user_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Usuario eliminado con éxito",
                        success: true,
                    });
            });
        });
    },
    createRestaurant: (root, { input }) => {
        const nuevoRestaurant = new restaurant_1.default({
            title: input.title,
            image: input.image,
            description: input.description,
            rating: input.rating,
            address: {
                calle: input.address.calle,
                numero: input.address.numero,
                codigoPostal: input.address.codigoPostal,
                ciudad: input.address.ciudad,
            },
            coordetate: input.coordetate,
            coordenateone: input.coordenateone,
            categoryName: input.categoryName,
            categoryID: input.categoryID,
            minime: input.minime,
            menu: input.menu,
            phone: input.phone,
            email: input.email,
            city: input.city,
            logo: input.logo,
            password: input.password,
            type: input.type,
            apertura: input.apertura,
            cierre: input.cierre,
            diaslaborales: input.diaslaborales,
            open: input.open,
            isnew: input.isnew,
        });
        return new Promise((resolve, rejects) => {
            nuevoRestaurant.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    eliminarRestaurant: (root, { id }) => {
        return new Promise((resolve, reject) => {
            restaurant_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Restaurante eliminado con éxito",
                        success: true,
                    });
            });
        });
    },
    actualizarRestaurant: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            restaurant_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un error con tu solicitud vuelve a intentalo por favor",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Datos actualizado con éxito",
                        success: true,
                    });
            });
        });
    }),
    crearFavorito: (root, { restaurantID, usuarioId }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        console.log(restaurantID, usuarioId);
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para comtinuar",
                success: false,
                data: null,
            };
        }
        const favorito = new Favorito_1.default({
            usuarioId,
            restaurantID,
        });
        favorito.id = favorito._id;
        return new Promise((resolve, reject) => {
            favorito.save((error) => {
                if (error) {
                    reject({
                        messages: "Hay un problema con tu solicitud",
                        success: false,
                    });
                    console.log(error);
                }
                else {
                    resolve({
                        messages: "Restaurante añadido a favorito",
                        success: true,
                    });
                    console.log("dome favorite");
                }
            });
        });
    }),
    eliminarFavorito: (root, { id }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para comtinuar",
                success: false,
            };
        }
        return new Promise((resolve, reject) => {
            Favorito_1.default.findOneAndDelete({ restaurantID: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con tu solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Restaurante eliminado de favorito",
                        success: true,
                    });
            });
        });
    }),
    createPlatos: (root, { input }) => {
        const newPlatos = new platos_1.default({
            title: input.title,
            ingredientes: input.ingredientes,
            price: input.price,
            imagen: input.imagen,
            menu: input.menu,
            restaurant: input.restaurant,
            oferta: input.oferta,
            popular: input.popular,
            news: input.news,
        });
        return new Promise((resolve, rejects) => {
            newPlatos.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    createMenu: (root, { input }) => {
        const newMenu = new menu_1.default({
            title: input.title,
            subtitle: input.subtitle,
            restaurant: input.restaurant,
        });
        return new Promise((resolve, rejects) => {
            newMenu.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    createAcompanante: (root, { input }) => {
        const newAcompanante = new acompanante_1.default({
            name: input.name,
            children: input.children,
            plato: input.plato,
            required: input.required,
            restaurant: input.restaurant,
        });
        return new Promise((resolve, rejects) => {
            newAcompanante.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    createItemCard: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        const existenPlato = yield card_1.default.findOne({
            platoID: input.platoID,
            userId: input.userId,
        });
        if (existenPlato) {
            return new Promise((resolve, reject) => {
                card_1.default.findOneAndUpdate({ _id: existenPlato._id }, input, { new: true }, (error, card) => {
                    if (error)
                        reject(error);
                    else {
                        resolve({
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            success: true,
                        });
                    }
                });
            });
        }
        else {
            const newCardItem = new card_1.default({
                userId: input.userId,
                restaurant: input.restaurant,
                plato: input.plato,
                platoID: input.platoID,
                total: input.total,
                extra: input.extra,
                complementos: input.complementos,
            });
            return new Promise((resolve, rejects) => {
                newCardItem.save((error) => {
                    if (error) {
                        rejects(error);
                    }
                    else {
                        resolve({
                            messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                            success: true,
                        });
                    }
                });
            });
        }
    }),
    actualizarCardItem: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            card_1.default.findOneAndUpdate({ _id: input._id }, input, { new: true }, (error, card) => {
                if (error)
                    reject(error);
                else
                    resolve(card);
            });
        });
    }),
    eliminarCardItem: (root, { id }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para comtinuar",
                success: false,
            };
        }
        return new Promise((resolve, reject) => {
            card_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con tu solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Producto eliminado de tu carrito",
                        success: true,
                    });
            });
        });
    }),
    crearCupon: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const nuevoCupon = new cupones_1.default({
                clave: input.clave,
                descuento: input.descuento,
                tipo: input.tipo,
            });
            return new Promise((resolve, reject) => {
                nuevoCupon.save((error, cupon) => {
                    if (error) {
                        console.log("resolvers -> mutation -> crearCupon -> error1 ", error);
                        return reject(error);
                    }
                    else {
                        return resolve(cupon);
                    }
                });
            });
        }
        catch (error) {
            console.log("resolvers -> mutation -> crearCupon -> error2 ", error);
            return new Promise((_resolve, reject) => {
                return reject({
                    success: false,
                    message: "Hay un problema con su solicitud",
                    data: null,
                });
            });
        }
    }),
    eliminarCupon: (root, { id }) => {
        return new Promise((resolve, reject) => {
            cupones_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        success: false,
                        message: "Hubo un problema con su solicitud",
                    });
                else
                    resolve({
                        success: true,
                        message: "Miembro eliminado con éxito",
                    });
            });
        });
    },
    crearModificarOrden: (root, { input }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            if (!usuarioActual) {
                return {
                    success: false,
                    message: "Debes iniciar sesión para continiar",
                    data: null,
                };
            }
            const usuario = yield user_1.default.findOne({
                _id: usuarioActual._id,
            });
            if (!usuario) {
                return {
                    success: false,
                    message: "Debes iniciar sesión para continiar",
                    data: null,
                };
            }
            let orden;
            if (input.id) {
                orden = yield order_1.default.findById(ObjectId(input.id)).exec();
            }
            if (!!input.restaurant) {
                const restaurants = yield restaurant_1.default
                    .findById(ObjectId(input.restaurant))
                    .exec();
                if (!restaurants) {
                    return new Promise((_resolve, reject) => {
                        return reject({
                            success: false,
                            message: "No existe un restaurante para esta orden",
                            data: null,
                        });
                    });
                }
            }
            let cupon;
            if (!!input.clave) {
                cupon = yield cupones_1.default.findOne({ clave: input.clave }).exec();
            }
            else if (!!input.cupon) {
                cupon = ObjectId(input.cupon);
            }
            let platos;
            platos = yield card_1.default.find({
                userId: input.userID,
                restaurant: input.restaurant,
            });
            if (!orden || !orden._id) {
                orden = new order_1.default({
                    restaurant: input.restaurant,
                    time: input.time,
                    cupon: cupon && cupon._id ? cupon._id : cupon,
                    nota: input.nota,
                    aceptaTerminos: input.aceptaTerminos,
                    cantidad: input.cantidad || 1,
                    userID: input.userID,
                    platos: platos,
                    cubiertos: input.cubiertos,
                    total: input.total,
                    propina: input.propina,
                    isvalored: input.isvalored,
                });
            }
            else {
                if (!orden.cupon) {
                    orden.set({
                        cupon: cupon && cupon._id ? cupon._id : cupon,
                    });
                }
            }
            return new Promise((resolve, reject) => {
                orden.save((error, ordenguardada) => {
                    if (error) {
                        console.log("resolvers -> mutation -> crearModificarOrden -> error1 ", error);
                        return reject(error);
                    }
                    else {
                        if (!ordenguardada.descuento && cupon && cupon._id) {
                            ordenguardada.descuento = cupon;
                        }
                        return resolve(ordenguardada);
                    }
                });
            });
        }
        catch (error) {
            console.log("resolvers -> mutation -> crearModificarOrden -> error2 ", error);
            return new Promise((_resolve, reject) => {
                return reject({
                    success: false,
                    message: "Hay un problema con tu solicitud",
                    data: null,
                });
            });
        }
    }),
    crearValoracion: (root, { input }) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const nuevoValoracion = new rating_1.default({
                user: input.user,
                comment: input.comment,
                value: input.value,
                restaurant: input.restaurant,
            });
            return new Promise((resolve, reject) => {
                nuevoValoracion.save((error, rating) => {
                    if (error) {
                        console.log("resolvers -> mutation -> crearCupon -> error1 ", error);
                        return reject(error);
                    }
                    else {
                        return resolve(rating);
                    }
                });
            });
        }
        catch (error) {
            console.log("resolvers -> mutation -> crearCupon -> error2 ", error);
            return new Promise((_resolve, reject) => {
                return reject({
                    success: false,
                    message: "Hay un problema con su solicitud",
                    data: null,
                });
            });
        }
    }),
    createNotification: (root, { input }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para continiar",
                success: false,
            };
        }
        let newNotification = new Notification_1.default({
            user: input.user,
            usuario: input.usuario,
            restaurant: input.restaurant,
            type: input.type,
            ordenId: input.ordenId,
        });
        return new Promise((resolve, reject) => {
            newNotification.save((error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Notificacion leída con éxito",
                        success: true,
                    });
            });
        });
    }),
    readNotification: (root, { notificationId }, { usuarioActual }) => __awaiter(void 0, void 0, void 0, function* () {
        if (!usuarioActual) {
            return {
                messages: "Debes iniciar sesión para continiar",
                success: false,
            };
        }
        return new Promise((resolve, reject) => {
            Notification_1.default.findOneAndUpdate({ _id: ObjectId(notificationId) }, { $set: { read: true } }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Notificacion leída con éxito",
                        success: true,
                    });
            });
        });
    }),
    createOpinion: (root, { input }) => {
        const nuevaOpinion = new Opinion_1.default({
            plato: input.plato,
            comment: input.comment,
            rating: input.rating,
            user: input.user,
        });
        return new Promise((resolve, rejects) => {
            nuevaOpinion.save((error) => {
                if (error) {
                    rejects(error);
                }
                else {
                    resolve({
                        messages: Status_messages_1.STATUS_MESSAGES.DATA_SUCCESS,
                        success: true,
                    });
                }
            });
        });
    },
    OrdenProceed: (root, { orden, estado, progreso, status }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            const message = estado === "Confirmada" ? "Confirmada" : "Rechazada";
            let dataToUpdate = { estado };
            //@ts-ignore
            if (progreso)
                dataToUpdate.progreso = progreso;
            //@ts-ignore
            if (status)
                dataToUpdate.status = status;
            order_1.default.findOneAndUpdate({ _id: orden }, dataToUpdate, (error) => {
                if (error) {
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                }
                else {
                    resolve({
                        messages: "Orden procesada con éxito",
                        success: true,
                    });
                }
            });
        });
    }),
    eliminarPlato: (root, { id }) => {
        return new Promise((resolve, reject) => {
            platos_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Plato eliminado con éxito",
                        success: true,
                    });
            });
        });
    },
    eliminarComplemento: (root, { id }) => {
        return new Promise((resolve, reject) => {
            acompanante_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Complemento eliminado con éxito",
                        success: true,
                    });
            });
        });
    },
    eliminarMenu: (root, { id }) => {
        return new Promise((resolve, reject) => {
            menu_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hubo un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Menú eliminado con éxito",
                        success: true,
                    });
            });
        });
    },
    crearPago: (root, { input }) => {
        const nuevoPago = new Pago_1.default({
            nombre: input.nombre,
            iban: input.iban,
            restaurantID: input.restaurantID,
        });
        nuevoPago.id = nuevoPago._id;
        return new Promise((resolve, reject) => {
            nuevoPago.save((error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Pago añadido con éxito",
                        success: true,
                    });
            });
        });
    },
    eliminarPago: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            Pago_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Pago eliminado con éxito",
                        success: true,
                    });
            });
        });
    }),
    crearDeposito: (root, { input }) => {
        const nuevoDeposito = new transacciones_1.default({
            fecha: new Date(),
            estado: input.estado,
            total: input.total,
            restaurantID: input.restaurantID,
        });
        nuevoDeposito.id = nuevoDeposito._id;
        return new Promise((resolve, reject) => {
            nuevoDeposito.save((error) => {
                if (error)
                    reject({
                        messages: "Hay un problema con su solicitud",
                        success: false,
                    });
                else
                    resolve({
                        messages: "Deposito añadido con éxito",
                        success: true,
                    });
            });
        });
    },
    eliminarDeposito: (root, { id }) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            transacciones_1.default.findOneAndDelete({ _id: id }, (error) => {
                if (error)
                    reject({
                        success: false,
                        messages: "Hay un problema con su solicitud",
                    });
                else
                    resolve({
                        success: true,
                        messages: "Deposito eliminado con éxito",
                    });
            });
        });
    }),
};
